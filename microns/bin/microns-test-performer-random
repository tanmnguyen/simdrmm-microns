#!/usr/bin/env python

# Copyright 2014, Sandia Corporation. Under the terms of Contract
# DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains certain
# rights in this software.

import argparse
import hashlib
import numpy
import sys

parser = argparse.ArgumentParser(description="""Sample MICrONS performer that
returns deterministic (re-running a trial with the same inputs produces the
same outputs), but random (the returned values have no relation to the input
contents) similarities.  Each similarity returned will be a value between 0 and
1.""")
arguments = parser.parse_args()

# Performers read their inputs from stdin.  The input will contain multiple
# trials, one per line, and each trial will be a comma-separated set of
# filesystem paths.  The first path for each trial is the "reference" image,
# and the remaining paths are the available image "choices" for that trial.
trials = [trial.strip().split(",") for trial in sys.stdin]

# For each trial:
similarities = []
for trial in trials:
    # Generate a random seed from the image paths.
    reference = trial[0]
    choices = trial[1:]

    seed = hashlib.md5(reference.encode())
    for choice in choices:
        seed.update(choice.encode())
    numpy.random.seed(int(seed.hexdigest()[:8], 16))

    # Generate scores (random similarity values in this case), one for each choice.
    similarities.append(numpy.random.uniform(size=len(choices)))

# Performers write their outputs to stdout.  The output must contain the same
# number of trials as the input, one per line.  Each trial is a comma-delimited
# set of similiarities - the similarity between the reference image and the
# first choice, the similarity between the reference image and the second
# choice, and-so-on.
sys.stdout.write("\n".join([",".join(["%.3f" % similarity for similarity in trial]) for trial in similarities]))

