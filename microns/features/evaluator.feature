Feature: MICrONS test evaluator

    Scenario Outline: MICrONS test evaluator.
        Given <images>
        And <performer>
        When running the test evaluator
        Then the evaluator should exit <exit>
        And stderr should contain <stderr>

        Examples:
            | images                                            | performer             | exit             | stderr                                      |
            | a directory that contains no images               | a random performer    | with an error    | "Not enough .png images to generate trials" |
            | a directory that contains invalid PNG images      | a random performer    | with an error    | "not a valid PNG image"                     |
            | a directory that contains grayscale PNG images    | a random performer    | with an error    | "Images must contain RGB data only."        |
            | a directory that contains RGBA PNG images         | a random performer    | with an error    | "Images must contain RGB data only."        |
            | a directory that contains RGB PNG images          | a random performer    | without an error | "Trial 9:"                                  |
            | a directory that contains RGB PNG images          | a histogram performer | without an error | "Trial 9:"                                  |

