from behave import *

import io
import nose.tools
import numpy
import os
import skimage.data
import skimage.exposure
import skimage.io
import subprocess
import sys
import tempfile

@given(u'a directory that contains no images')
def step_impl(context):
    context.images = tempfile.mkdtemp()


@given(u'a directory that contains invalid PNG images')
def step_impl(context):
    context.images = tempfile.mkdtemp()
    for i in range(6):
        with open(os.path.join(context.images, "test-%s.png" % i), "wb") as image:
            image.write(b"I'm not really a PNG file.")


@given(u'a directory that contains grayscale PNG images')
def step_impl(context):
    context.images = tempfile.mkdtemp()
    data = skimage.data.camera()
    for i in range(6):
        skimage.io.imsave(os.path.join(context.images, "test-%s.png" % i), data)

@given(u'a directory that contains RGBA PNG images')
def step_impl(context):
    context.images = tempfile.mkdtemp()
    data = skimage.data.astronaut()
    alpha = numpy.ones((data.shape[0], data.shape[1], 1), dtype=data.dtype)
    data = numpy.concatenate((data, alpha), axis=2)
    for i in range(6):
        skimage.io.imsave(os.path.join(context.images, "test-%s.png" % i), data)


@given(u'a directory that contains RGB PNG images')
def step_impl(context):
    context.images = tempfile.mkdtemp()
    sys.stderr.write("%s\n" % context.images)
    skimage.io.imsave(os.path.join(context.images, "astronaut.png"), skimage.data.astronaut())
    skimage.io.imsave(os.path.join(context.images, "chelsea.png"), skimage.data.chelsea())
    skimage.io.imsave(os.path.join(context.images, "coffee.png"), skimage.data.coffee())
    skimage.io.imsave(os.path.join(context.images, "hubble_deep_field.png"), skimage.data.hubble_deep_field())
    skimage.io.imsave(os.path.join(context.images, "rocket.png"), skimage.data.rocket())
    skimage.io.imsave(os.path.join(context.images, "gamma.png"), skimage.exposure.adjust_gamma(skimage.data.rocket(), gamma=2.2))


@given(u'a random performer')
def step_impl(context):
    context.performer = "microns-test-performer-random"


@given(u'a histogram performer')
def step_impl(context):
    context.performer = "microns-test-performer-histogram"


@when(u'running the test evaluator')
def step_impl(context):
    command = ["microns-test-evaluator", context.images, context.performer]
    process = subprocess.Popen(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    context.stdout, context.stderr = process.communicate()
    context.returncode = process.returncode


@then(u'the evaluator should exit with an error')
def step_impl(context):
    nose.tools.assert_not_equal(context.returncode, 0)


@then(u'the evaluator should exit without an error')
def step_impl(context):
    nose.tools.assert_equal(context.returncode, 0)


@then(u'stderr should contain {message}')
def step_impl(context, message):
    nose.tools.assert_in(eval(message).encode(), context.stderr)


