# Testing Tools for MICrONS Performers

The enclosed software is designed to help MICrONS performers test their
algorithms to ensure compatibility with the evaluation process. We provide
several Python executables for testing.  The first,
`microns-test-evaluator`, runs a performer algorithm to verify that it
accepts the correct inputs and produces the correct outputs, as specified at
kickoff.  The other executables are working (but hopelessly naive) examples
that demonstrate how performer algorithms must behave.

## Changelog

**September 26, 2016 - Version 0.2.0**

Updated the performers to accept more than one trial as input, so load times
can be amortized.  Trials will be sent to the performer over stdin, one trial
per line, as a comma-delimited set of absolute file paths.  As before, the
first path is the reference image and the remaining paths are the choice images
for that trial.

Similarly, performers must return their outputs using stdout, one trial per
line, as a comma-delimited set of floating-point similarity / dissimilarity
scores.  Each score in the output will be the comparison between the input
reference image and one of input choice images, in the same order.  Thus, the
number of scores in the output will be equal to the number of choices in the
input, i.e. if the inputs contain N columns, the outputs will contain N-1
columns.

Performers should not make any assumptions about the
number of trials that will be received as input.


**June 24, 2016 - Version 0.1.0**

Initial release, which assumed that we would run each performer once for every
trial.

## Dependencies

The following are required to run the test executables:

* Python 2.7 / Python 3
* Python `setuptools` module.
* Python `numpy` module.
* Python `scipy` module.
* Python `scikit-image` module.
* A directory containing images in PNG format.  The filenames *must* end in `.png`.

## Getting Started

* Install the required dependencies using your package manager and/or `pip`.
* Install the test executables using the `setup.py` script:

        $ python setup.py develop

* Try running the test evaluator with a random algorithm:

        $ microns-test-evaluator /path/to/images microns-test-performer-random
        INFO: Found 3691 .png images in the /path/to/images directory.
        INFO: Batch 0 Trial 0: 0.618 0.644 0.943 0.024 0.075 0.711
        INFO: Batch 0 Trial 1: 0.918 0.153 0.757 0.204 0.745 0.438
        INFO: Batch 0 Trial 2: 0.155 0.488 0.718 0.689 0.497 0.125
        INFO: Batch 0 Trial 3: 0.849 0.164 0.112 0.274 0.49 0.544
        INFO: Batch 0 Trial 4: 0.795 0.49 0.924 0.509 0.016 0.088
        INFO: Batch 1 Trial 5: 0.212 0.618 0.067 0.843 0.403 0.653
        INFO: Batch 1 Trial 6: 0.865 0.829 0.921 0.873 0.681 0.823
        INFO: Batch 1 Trial 7: 0.002 0.129 0.992 0.849 0.747 0.021
        INFO: Batch 1 Trial 8: 0.637 0.782 0.535 0.116 0.784 0.091
        INFO: Batch 1 Trial 9: 0.047 0.745 0.759 0.746 0.355 0.112

In this case, the test evaluator creates ten trials, split into two batches.
For each trial, it draws a reference image and six choices (one of which is a
match for the reference image) at random from the image directory you provided.
The number of batches, trials, and choices is configurable using command-line
arguments.  The performer is run twice, once for each batch.  Image paths are
passed as absolute filepaths to the performer, which returns a similarity /
dissimilarity measure for each pairing of the reference image with a choice
image, which the evaluator logs to stderr.  In this case, the naive performer
algorithm simply returns a random similarity in the range [0, 1] for each
pairing of reference and choice images.

* Try running the test evaluator with a not-so-random algorithm:

        $ microns-test-evaluator /path/to/images microns-test-performer-histogram
        INFO: Found 3691 .png images in the /path/to/images directory.
        INFO: Batch 0 Trial 0: 0.0 0.0 0.0 0.0 1.0 0.0
        INFO: Batch 0 Trial 1: 0.0012 0.0 0.0 0.0 1.0 0.0
        INFO: Batch 0 Trial 2: 0.0 0.0258 1.0 0.0009 0.1342 0.0002
        INFO: Batch 0 Trial 3: 0.0 0.0 0.0 0.0 1.0 0.0017
        INFO: Batch 0 Trial 4: 0.0 0.0 0.0 0.0 0.1342 1.0
        INFO: Batch 1 Trial 5: 0.0 0.0 0.0 0.0 0.0 1.0
        INFO: Batch 1 Trial 6: 0.0 0.0 0.0 1.0 0.0 0.0
        INFO: Batch 1 Trial 7: 0.0 0.0 0.0 0.0 1.0 0.0
        INFO: Batch 1 Trial 8: 0.0 1.0 0.0 0.0001 0.0 0.0
        INFO: Batch 1 Trial 9: 1.0 0.0 0.0 0.0 0.0 0.0

This algorithm also returns a similarity measure in the range [0, 1], based on
the probability that the luminance histograms of two images are drawn from the
same distributions.

* You can run any of the executables with the `--help` option for information on additional command line arguments.

* When you’re ready, run `microns-test-evaluator` with your own machine learning algorithms, to verify that they work correctly.

## Contacts

* Tim Shead, Sandia National Laboratories - tshead@sandia.gov
* Warren Davis, Sandia National Laboratories - wldavis@sandia.gov
