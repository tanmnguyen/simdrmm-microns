# Copyright 2014, Sandia Corporation. Under the terms of Contract
# DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains certain
# rights in this software.

from setuptools import setup, find_packages
import re

setup(
    description = "MICrONS test evaluator and performers.",
    install_requires = [
        "numpy>=1.7",
        "scikit-image",
        "scipy",
        ],
    maintainer_email = "tshead@sandia.gov",
    maintainer = "Timothy M. Shead",
    name = "microns",
    scripts = [
        "bin/microns-test-evaluator",
        "bin/microns-test-performer-histogram",
        "bin/microns-test-performer-random",
        ],
    packages=find_packages(),
    version=re.search(r"^__version__ = ['\"]([^'\"]*)['\"]", open("microns/__init__.py", "r").read(), re.M).group(1),
)
