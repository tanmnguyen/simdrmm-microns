# Copyright 2014, Sandia Corporation. Under the terms of Contract
# DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains certain
# rights in this software.

from setuptools import setup, find_packages
import re

setup(
    description = "Siamese DRMM.",
    install_requires = [
        "numpy>=1.7",
        "scikit-image",
        "scipy",
        "scikit-learn",
        "dill"
        ],
    maintainer_email = "",
    maintainer = "Tan Nguyen, Wanjia Liu",
    name = "simdrmm",
    scripts = [
        "scripts/microns-test-performer-drmm",
        ],
    packages=['simdrmm'],
    version='0.1.0',
)
