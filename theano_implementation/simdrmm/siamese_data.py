import numpy as np
import random
import scipy.stats
import time
np.set_printoptions(threshold=np.nan)
class SiameseData:
    def __init__(self, path, num_labeled=1000):
        self.path = path
        self.num_labeled = num_labeled
        self.load_data()
        
    def load_data(self, channel=1):
        """
        Function to load the raw images, which are already splited
        into training, testing and validation parts
        """
        self.raw_data = np.load(self.path)
        if "X_valid" not in self.raw_data:
            # use training data as validation data
            print "Validation data doesn't exist, use part of training" + \
                    " data instead." 
            x_valid = self.raw_data["X_train"][0:self.raw_data["X_test"].shape[0],:]
            y_valid = (self.raw_data["Y_train"][0:self.raw_data["Y_test"].shape[0],:]).astype(np.int32)
            yg_valid = (self.raw_data["Yg_train"][0:self.raw_data["Yg_test"].shape[0],:]).astype(np.int32)
 
            self.X = [self.raw_data["X_train"], self.raw_data["X_test"], x_valid]
            self.Y = [self.raw_data["Y_train"].astype(np.int32), self.raw_data["Y_test"].astype(np.int32), y_valid]
            self.Yg = [self.raw_data["Yg_train"].astype(np.int32), self.raw_data["Yg_test"].astype(np.int32), yg_valid]

        else:
            self.X = [self.raw_data["X_train"], self.raw_data["X_test"], \
                    self.raw_data["X_valid"]]
            self.Y = [self.raw_data["Y_train"].astype(np.int32), self.raw_data["Y_test"].astype(np.int32), \
                    self.raw_data["Y_valid"].astype(np.int32)]
            self.Yg = [self.raw_data["Yg_train"].astype(np.int32), self.raw_data["Yg_test"].astype(np.int32), \
                    self.raw_data["Yg_valid"].astype(np.int32)]

        for index, split_type in zip(range(3), ["train", "test", "val"]):
            print split_type + " data loaded:"
            print "     X size:", self.X[index].shape
            print "     Y size:", self.Y[index].shape
            print "     Yg size:", self.Yg[index].shape
            print "     Specific label count:", np.unique(self.Y[index]).size
            print "     Generic label count", np.unique(self.Yg[index]).size
            print "     Generic label frequency count:"
            c = np.bincount(self.Yg[index].flatten())
            i = np.nonzero(c)[0]
            print np.vstack((i, c[i])).T.flatten()
        print "Specific label overlap between train and test:", len( \
                set(self.Y[0].flatten().tolist()).intersection(set(self.Y[1].flatten().tolist()))) 
        print "Specific label overlap between train and valid:", len( \
                set(self.Y[0].flatten().tolist()).intersection(set(self.Y[2].flatten().tolist()))) 

        self.num_specific_labels = np.unique(np.vstack(self.Y)).size
        self.num_generic_labels = np.unique(np.vstack(self.Yg)).size
   
        print "Total number of specific labels:", self.num_specific_labels 
        print "Total number of generic labels:", self.num_generic_labels 


        self.channel = int(channel)
        self.dim = int(np.sqrt(self.X[0].shape[1] / self.channel))

        # labeled training data
        self.labeled_training_data_index = np.random.choice(\
                range(self.X[0].shape[0]), self.num_labeled, replace=False)
        self.labeled_training_data = self.X[0][self.labeled_training_data_index, :]
        self.labeled_training_data_Y = self.Y[0][self.labeled_training_data_index, :]
        self.labeled_training_data_Yg = self.Yg[0][self.labeled_training_data_index, :]
        self.labeled_data_cursor = 0

    def get_pair(self, batch_size, ratio, split, label_type, K=None):
        """
        Get a pair of images and their similarity labels
        Args:
        batch_size: batch_size of pair
        split: 0 for training split, 1 for test split, 2 for validation split
        label_type: specific or generic labels
        K: Number of labels that will be used to form pairs.
        ratio: positive to negative ratio
        Returns:
        image_first: first images in the pair, of batch_size
        image_second: second images in the pair, of batch_size
        similarity: similarity label, could be 0 or 1, of batch_size 
        """
        debug_info = {}
        X = self.X[split]

        if label_type == "specific":
            Y = self.Y[split]
        elif label_type == "generic":
            Y = self.Yg[split]

        if K is None:
            K = np.unique(Y).size
 
        print "Requesting pairs from " + str(K) + " " + label_type + " labels."

        label_selected = np.random.choice(Y.flatten(), \
                    K, replace=False)
        debug_info["label_selected"] = label_selected

        indices_first = []
        indices_second = []
        for i in range(batch_size):
            if random.random() < ratio:
                first_label = np.random.choice(label_selected.flatten(),
                    1,replace=True)[0]
                second_label = first_label
            else:
                first_and_second_label = np.random.choice(label_selected.flatten(), 2,\
                     replace=False)
                first_label = first_and_second_label[0]
                second_label = first_and_second_label[1]

            first_indices_to_use = ((Y[:,0] == first_label).nonzero())[0]
            indices_first.append(first_indices_to_use[np.random.randint(0, np.size(first_indices_to_use))])
            second_indices_to_use = ((Y[:, 0] == second_label).nonzero())[0]
            indices_second.append(second_indices_to_use[np.random.randint(0, np.size(second_indices_to_use))])

        debug_info["indices_first"] = indices_first
        debug_info["indices_second"] = indices_second
        
        images_first = X[indices_first,:]
        images_second = X[indices_second,:]
        similarity = np.equal(Y[indices_first,:], Y[indices_second,:])
        similarity = (similarity.astype(np.int32)).reshape(-1)

        # labeled data
        random_samples = self.labeled_training_data.take(\
                range(self.labeled_data_cursor,self.labeled_data_cursor + batch_size),\
                axis=0, mode="wrap")
        random_samples_labels = self.labeled_training_data_Y.take(\
                range(self.labeled_data_cursor,self.labeled_data_cursor + batch_size),\
                axis=0, mode="wrap")
        self.labeled_data_cursor = (self.labeled_data_cursor + batch_size) % self.num_labeled
        
        return_dict = {
                "input1": images_first.reshape( \
                        [batch_size, self.channel, self.dim, self.dim]),
                "input2": images_second.reshape( \
                        [batch_size, self.channel, self.dim, self.dim]),
                "siamese-labels": similarity.flatten().transpose(),
                "input-lab": random_samples.reshape( \
                        [batch_size, self.channel, self.dim, self.dim]),
                "obj-labels": random_samples_labels.flatten().transpose()}

        return return_dict, debug_info

    def get_choices(self, batch_size, split, label_type, K=None, num_choices=2):
        """
        Get a reference and choices to perform similarity benchmark
        Args:
        batch_size: batch_size of pair
        split: 0 for training split, 1 for test split, 2 for validation split
        label_type: specific or generic labels
        K: Number of labels that will be used to form pairs. Larger K means
            less ratio of positive pairs. K = 2 will results in 1:1 positive
            to negative pair ratio.
        num_choices: number of choices, default is 2
        Returns:
        image_ref: reference images, of batch_size
        image_choices: a list of choices images, of batch_size and of num_choices
        answer: index of image choices that best match reference image
        """
        debug_info = {}
        X = self.X[split]

        if label_type == "specific":
            Y = self.Y[split]
        elif label_type == "generic":
            Y = self.Yg[split]

        if K is None:
            K = np.unique(Y).size
 
        print "Requesting " + str(K) + " " + label_type + " labels."

        label_selected = np.random.choice(Y.flatten(), \
                    K, replace=False)
        debug_info["label_selected"] = label_selected
        # reference label to use
        ref_label = np.random.choice(label_selected, batch_size, replace=True)
        debug_info["ref_label"] = ref_label
        
        indices_ref = []
        indices_true = []
        indices_false = []
        for j in range(num_choices-1):
            indices_false.append([])

        for i in range(batch_size):
            # test labels to use
            test_label_choices = list(set(label_selected.tolist()) - set([ref_label[i]]))
            test_labels = np.random.choice(test_label_choices, num_choices-1, replace=True)
            # indices of images that are references
            ref_indices_to_use = ((Y[:,0] == ref_label[i]).nonzero())[0]
            
            # image index choice of reference
            indices_ref.append(ref_indices_to_use[np.random.randint(\
                    0, np.size(ref_indices_to_use))])
            # image index choice of correct choice
            indices_true.append(ref_indices_to_use[np.random.randint(\
                    0, np.size(ref_indices_to_use))])
     
            # image index choice of incorrect choice
            for j in range(num_choices-1):
                test_indices_to_use = ((Y[:,0] == test_labels[j]).nonzero())[0]
                indices_false[j].append(test_indices_to_use[np.random.randint(\
                        0, np.size(test_indices_to_use))])

        assert len(indices_ref) == batch_size 
        assert len(indices_true) == batch_size 
        assert len(indices_false) == num_choices-1

        for i in indices_false:
            assert len(i) == batch_size

        debug_info["indices_ref"] = indices_ref
        debug_info["indices_true"] = indices_true
        debug_info["indices_false"] = indices_false
        image_ref = np.zeros((batch_size, X.shape[1])).astype(np.float32)     
        image_choices = [ np.zeros((batch_size, X.shape[1])).astype(np.float32) \
                for _ in range(num_choices)]
        answer = np.zeros((batch_size, 1)).astype(np.float32)
        
        for i in range(batch_size):
            ans = random.choice(range(num_choices))
            answer[i,0] = ans
            image_ref[i,:] = X[indices_ref[i],:]
            false_index = 0
            for choice in range(num_choices):
                if choice == 0:
                    image_choices[choice][i,:] = X[indices_true[i],:]
                else:
                    image_choices[choice][i,:] = X[indices_false[false_index][i],:]
                    false_index += 1
            assert false_index == (num_choices-1)
        
        assert num_choices == 2

        return_dict = { \
                "reference": image_ref.reshape([batch_size, self.channel, \
                        self.dim, self.dim]),
                "positive": image_choices[0].reshape([batch_size, self.channel, \
                        self.dim, self.dim]),
                "negative": image_choices[1].reshape([batch_size, self.channel, \
                        self.dim, self.dim])}
        return return_dict, debug_info

######################## User Functions ###########################
    def get_training_pair(self, batch_size, ratio=0.5, label_type="specific", K=None):
        return self.get_pair(batch_size, ratio , 0, label_type, K)

    def get_validation_triplet(self, batch_size, label_type="specific", K=None,num_choices=2):
        return self.get_choices(batch_size, 2, label_type, K, num_choices)

    def get_test_triplet(self, batch_size, label_type="specific", K=None,num_choices=2):
        return self.get_choices(batch_size, 1, label_type, K, num_choices)

######################## Unit Tests ################################
    def run_unit_test(self):
        self.unit_test_get_pair()
        self.unit_test_get_triplet()
        print "All Unit Test passed!"

    def unit_test_get_pair(self):
        bs = 10

        start = time.time()
        return_dict, debug_info = self.get_training_pair(bs) 
        end = time.time()

        assert self.labeled_data_cursor == bs 
               
        print "Get Training Pair elapsed time in second:", (end-start)
        first = return_dict["input1"]
        second = return_dict["input2"]
        similarity = return_dict["siamese-labels"]
        samples = return_dict["input-lab"]  
        sample_labels = return_dict["obj-labels"] 
         
        assert first.shape == (bs, self.channel, self.dim, self.dim)
        assert second.shape == (bs, self.channel, self.dim, self.dim)
        assert similarity.shape == (bs, )
        assert samples.shape == (bs, self.channel, self.dim, self.dim)
        assert sample_labels.shape == (bs, )
       
        # check each pair similarity based on label is correctly computed 
        for i in range(len(debug_info["indices_first"])):
            test = self.Y[0][debug_info["indices_first"][i],0] == \
                    self.Y[0][debug_info["indices_second"][i],0]
            #print i,test, similarity[i,0], \
            #        self.Y[0][debug_info["indices_first"][i],0],\
            #        self.Y[0][debug_info["indices_second"][i],0]

            assert int(test) == similarity[i,]
    
    def unit_test_get_triplet(self):
        nc = 2
        bs = 10
        start = time.time()
        return_dict, debug_info = self.get_validation_triplet(bs,\
                num_choices=nc)
        end=time.time()
        print "Get triplet elapsed time in second:", (end-start)
        ref = return_dict["reference"]
        pos = return_dict["positive"]
        neg = return_dict["negative"]

        assert ref.shape == (bs, self.channel, self.dim, self.dim)
        assert pos.shape == (bs, self.channel, self.dim, self.dim)
        assert neg.shape == (bs, self.channel, self.dim, self.dim)
        
        indices_ref = debug_info["indices_ref"]
        indices_true = debug_info["indices_true"]
        indices_false = debug_info["indices_false"]

        for i in range(len(indices_ref)):
            temp = 0 
            for j in range(nc):
                if j == 0:
                    assert self.Y[2][indices_ref[i],0] == \
                        self.Y[2][indices_true[i],0]
                else:
                    assert not (self.Y[2][indices_false[temp][i],0] == \
                        self.Y[2][indices_ref[i],0])
                    temp += 1
            assert temp == (nc -1)
 
if __name__ == "__main__":
    data_block = "/home/ubuntu/s3-shapenet/data_split_preprocessed.npz"
    #data_block = "/home/ubuntu/s3-shapenet/data_preprocessed.npz"
    data = SiameseData(data_block, num_labeled=1000)
    data.run_unit_test()

           
     



            
        


