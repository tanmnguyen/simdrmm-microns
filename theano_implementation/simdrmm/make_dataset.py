import numpy as np
import glob
import scipy.misc
import os
import pickle
import sklearn.utils
import random
import shutil

np.set_printoptions(threshold=np.nan)
def get_image(image_path, image_size, is_crop=False, resize_w=64, is_grayscale = False):
    return transform(imread(image_path, is_grayscale), image_size, is_crop, resize_w)
def center_crop(x, crop_h, crop_w=None, resize_w=64):
    if crop_w is None:
        crop_w = crop_h
    h, w = x.shape[:2]
    j = int(round((h - crop_h)/2.))
    i = int(round((w - crop_w)/2.))
    #print i,j
    return scipy.misc.imresize(x[j:j+crop_h, i:i+crop_w],
                               [resize_w, resize_w])
def imread(path, is_grayscale = False):
    if (is_grayscale):
        return scipy.misc.imread(path, flatten = True).astype(np.float)
    else:
        return scipy.misc.imread(path).astype(np.float)

def transform(image, npx=64, is_crop=False, resize_w=64):
    # npx : # of pixels width/height of image
    if is_crop:
        cropped_image = center_crop(image, npx, resize_w=resize_w)
    else:
        cropped_image = center_crop(image, npx, resize_w=resize_w)
    return np.array(cropped_image)

def label_mapping(parent_path):
    sub_folders = glob.glob(os.path.join(parent_path,"*/" ))
    hash_to_label = {}
    next_label = 0
    next_general_label = 0
    Ys = []
    Ygs = []
    mapping = {}
    for subfolder in sub_folders:
        print "Subfolder :", subfolder
        classes = glob.glob(os.path.join(subfolder, "*/"))
        for cls in classes:
            print "    Class :", cls
            images = glob.glob(os.path.join(cls, "*.png"))
            for image in images:
                fn = os.path.basename(image)
                fn_hash = fn.split("_")[0]
                fn_index = fn.split("_")[1]

                # map hash to label 
                if fn_hash not in hash_to_label:
                    hash_to_label[fn_hash] = next_label
                    next_label += 1
                Ys.append(hash_to_label[fn_hash])
                Ygs.append(next_general_label)
                mapping[hash_to_label[fn_hash]] = next_general_label              
            next_general_label += 1
            #break
        #break
    Y = np.vstack(Ys)
    Yg = np.vstack(Ygs)
    return mapping 

def make_even_generic_nonoverlap_specific_split(parent_path):
    sub_folders = glob.glob(os.path.join(parent_path,"*/" ))
    hash_to_label = {}
    hash_to_general_label = {}
    next_label = 0
    next_general_label = 0
    split = [[] for _ in range(3)]
    i=0
    hash_to_split={}
    for subfolder in sub_folders:
        print "Subfolder :", subfolder
        classes = glob.glob(os.path.join(subfolder, "*/"))
        for cls in classes:
            print "    Class :", cls
            images = glob.glob(os.path.join(cls, "*.png"))
            for image in images:
                fn = os.path.basename(image)
                fn_hash = fn.split("_")[0]

                # map hash to label 
                if fn_hash not in hash_to_label:
                    hash_to_label[fn_hash] = next_label
                    next_label += 1
                    if i<3:
                        split[0].append(fn_hash)
                    elif i==3:
                        split[1].append(fn_hash)
                    else:        
                        split[2].append(fn_hash)
                    i  = (i + 1)%5
                                  
                if fn_hash not in hash_to_general_label:
                    hash_to_general_label[fn_hash] = next_general_label
            
            next_general_label += 1
            #break
        #break
    print len(split[0])
    print len(split[1])
    print len(split[2])
    print "Total specific labels:", next_label

    for index, split_name in zip(range(3), ["train", "test", "val"]):
        with open(os.path.join(parent_path, split_name+".txt"), "w") as fp:
            fp.write("\n".join(split[index]))
            fp.write("\n")

    
 
def create_dataset(parent_path,imsize=250, resize=64):
    sub_folders = glob.glob(os.path.join(parent_path,"*/" ))
    hash_to_label = {}
    next_label = 0
    next_general_label = 0
    Xs = []
    Ys = []
    Ygs = []
    for subfolder in sub_folders:
        print "Subfolder :", subfolder
        classes = glob.glob(os.path.join(subfolder, "*/"))
        for cls in classes:
            print "    Class :", cls
            images = glob.glob(os.path.join(cls, "*.png"))
            for image in images:
                fn = os.path.basename(image)
                fn_hash = fn.split("_")[0]
                fn_index = fn.split("_")[1]

                # map hash to label 
                if fn_hash not in hash_to_label:
                    hash_to_label[fn_hash] = next_label
                    next_label += 1
                Ys.append(hash_to_label[fn_hash])
                Ygs.append(next_general_label) 
                # read image
                img_read = get_image(image, imsize, resize_w=resize) 
                #print img_read.flatten().shape
                img_read.resize((1, resize * resize))
                #print img_read.shape
                Xs.append(img_read)
            next_general_label += 1
            #break
        #break
    #Xs, Ys = sklearn.utils.shuffle(Xs, Ys)
    X = np.vstack(Xs)
    Y = np.vstack(Ys)
    Yg = np.vstack(Ygs)
    index = range(X.shape[0])
    random.shuffle(index)
    X = X[index, :] 
    Y = Y[index, :] 
    Yg = Yg[index, :] 
 
    print X.shape, Y.shape
    return X, Y, Yg 

def seperate_split(parent_path, cp_path, split_file):

    #read train test val
    fp = open(os.path.join(parent_path, split_file), 'r')
    split = fp.readlines()
    split = set([i[0:len(i)-1] for i in split])
    
    # make sure data splits does not overlap
    sub_folders = glob.glob(os.path.join(parent_path,"*/" ))
    counter = 0
    for subfolder in sub_folders:
        print "Subfolder :", subfolder
        classes = glob.glob(os.path.join(subfolder, "*/"))
        for cls in classes:
            print "    Class :", cls
            images = glob.glob(os.path.join(cls, "*.png"))
            for image in images:
                fn = os.path.basename(image)
                fn_hash = fn.split("_")[0]
                fn_index = fn.split("_")[1]
                if fn_hash in split:
                    #shutil.copyfile(image, os.path.join(cp_path,fn))
                    shutil.copyfile(image, os.path.join(cp_path,str(counter).zfill(6)+".png"))
                    counter += 1
    print "Total number of images copied:", counter
    return

 
def create_dataset_with_split(parent_path, imsize=250, resize=64):

    #read train test val
    fps_to_parse = [open(os.path.join(parent_path, a + ".txt"), 'r') \
            for a in ["train", "test", "val"]]
    model_splits = []
    for fp in fps_to_parse:
        split = fp.readlines()
        split = set([i[0:len(i)-1] for i in split])
        model_splits.append(split)
    # print model_splits
    # make sure data splits does not overlap
    assert 0 == len(model_splits[0].intersection(model_splits[1]))
    assert 0 == len(model_splits[0].intersection(model_splits[2]))
    assert 0 == len(model_splits[1].intersection(model_splits[2]))
    print "Total: ", len(model_splits[0].union(model_splits[1]).union(model_splits[2]))
    sub_folders = glob.glob(os.path.join(parent_path,"*/" ))
    hash_to_label = {}
    next_label = 0
    next_general_label = 0
    Xs = [[] for split in model_splits]
    Ys = [[] for split in model_splits]
    Ygs = [[] for split in model_splits]
    for subfolder in sub_folders:
        print "Subfolder :", subfolder
        classes = glob.glob(os.path.join(subfolder, "*/"))
        for cls in classes:
            print "    Class :", cls
            images = glob.glob(os.path.join(cls, "*.png"))
            for image in images:
                fn = os.path.basename(image)
                fn_hash = fn.split("_")[0]
                fn_index = fn.split("_")[1]

                # map hash to label 
                if fn_hash not in hash_to_label:
                    hash_to_label[fn_hash] = next_label
                    next_label += 1

                # read image
                img_read = get_image(image, imsize, resize_w=resize) 
                #print img_read.flatten().shape
                img_read.resize((1, resize * resize))
                #print img_read.shape
                for split_i in range(len(model_splits)):
                    split = model_splits[split_i]
                    if fn_hash in split:
                        # print split_i
                        Xs[split_i].append(img_read)
                        Ys[split_i].append(hash_to_label[fn_hash])
                        Ygs[split_i].append(next_general_label) 
 
            next_general_label += 1
            #break
        #break

    #Xs, Ys = sklearn.utils.shuffle(Xs, Ys)
    X = [ np.vstack(i) for i in Xs ]
    Y = [ np.vstack(i) for i in Ys ]
    Yg = [ np.vstack(i) for i in Ygs]
    print "Unique Ys:", np.unique(np.vstack(Y)).size 
    print "Unique Ygs:", np.unique(np.vstack(Yg)).size 
    return X, Y, Yg


def find_generic_mapping():
    mapping = label_mapping("/home/ubuntu/stimulus/")
    # print mapping 
    raw_64 = np.load("/home/ubuntu/s3-shapenet/data_preprocessed.npz")
    print raw_64.keys()
    train_s = raw_64["Y_train"]
    train_g = np.zeros(train_s.shape)
    for i in range(train_s.shape[0]):
        train_g[i,0] = mapping[train_s[i,0]]
    #print train_g
    print max(train_g)

    test_s = raw_64["Y_test"]
    test_g = np.zeros(test_s.shape)
    for i in range(test_s.shape[0]):
        test_g[i,0] = mapping[test_s[i,0]]
    #print test_g
    print max(test_g)

    np.savez("/home/ubuntu/s3-shapenet/data_preprocessd_with_class.npz", \
        X_train=raw_64["X_train"], X_test=raw_64["X_test"],\
        Y_train=raw_64["Y_train"], Y_test=raw_64["Y_test"],\
        Yg_train=train_g, Yg_test=test_g) 
            
if __name__ == "__main__":
    # make_even_generic_nonoverlap_specific_split("/home/ubuntu/stimulus/")
    seperate_split("/home/ubuntu/stimulus/", "/home/ubuntu/stimulus/test_images/", "test.txt")
    #X, Y, Yg = create_dataset_with_split("/home/ubuntu/stimulus/", resize=64)
    
    
    #to_dump={}
    #to_dump["X"] = X
    #to_dump["Y"] = Y
    #to_dump["Yg"] = Yg
    
    # save un-preprocessed data
    #pickle.dump(to_dump, open("/home/ubuntu/shapenet-data/data_split_remake.pkl", "wb"))
