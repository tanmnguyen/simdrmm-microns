import os
import pickle

import numpy as np
from PIL import Image

import my_utils
from pylearn2.datasets import preprocessing


def read_data(dataset_path, save_path, num_train_image=50000, num_test_image=10000,if_save=True):
    if os.path.exists(os.path.join(save_path, "X_train_raw.npy")) and os.path.exists(os.path.join(save_path, "X_test_raw.npy")):
        print "dataset already exist!"
        X_train = np.load(os.path.join(save_path, "X_train_raw.npy"))
        X_test = np.load(os.path.join(save_path, "X_test_raw.npy"))
        return X_train, X_test
    else:
        train_data_shape = [num_train_image, 3072]
        test_data_shape = [num_test_image, 3072]
        image_prefix = "Frame_"
        image_suffix = ".png"

        X_train = np.zeros(train_data_shape)
        X_test = np.zeros(test_data_shape)

        # process images, training set
        for i in range(train_data_shape[0]):
            if i % 1000 == 0:
                print "Processing training set image ", i
            img_file = image_prefix + str(i) + image_suffix
            with Image.open(os.path.join(dataset_path, "blender_out", img_file)) as img:
                if img.mode=="RGB" or img.mode=="RGBA":
                    shape = (3, img.width, img.height)
                else:
                    shape= (1, img.width, img.height)
                matrix = my_utils.read_image_from_pil_load(img.load(), shape)
                matrix = matrix.reshape([matrix.size,])

                X_train[i, :] =  matrix       
        for i in range(test_data_shape[0]):
            if i % 1000 == 0:
                print "Processing testing set image ", i
            img_file = image_prefix + str(i + train_data_shape[0]) + image_suffix
            with Image.open(os.path.join(dataset_path, "blender_out", img_file)) as img:
                if img.mode=="RGB" or img.mode=="RGBA":
                    shape = (3, img.width, img.height)
                else:
                    shape= (1, img.width, img.height)
                matrix = my_utils.read_image_from_pil_load(img.load(), shape)
                matrix = matrix.reshape([matrix.size,])

                X_test[i, :] = matrix
        X_train=X_train.astype('float32').reshape([X_train.shape[0],3,32,32]).transpose([0,1,3,2])
        X_test=X_test.astype('float32').reshape([X_test.shape[0],3,32,32]).transpose([0,1,3,2])

        X_train=X_train.reshape([X_train.shape[0],3072])
        X_test=X_test.reshape([X_test.shape[0],3072])
 
        if if_save:
            np.save(os.path.join(save_path, "X_train_raw.npy"),X_train)
            np.save(os.path.join(save_path, "X_test_raw.npy"), X_test)
        return X_train, X_test

def forward_process(X_train, X_test, save_path, X_valid=None, scale = 128, tag=""):

    min_divisor = 1e-8

    # GCN
    scale = float(scale)

    X_train_mean = X_train.mean(axis=1, keepdims=True)
    X_test_mean = X_test.mean(axis=1, keepdims=True)

    X_train = X_train - X_train_mean
    X_test = X_test - X_test_mean

    X_train_normalizer = np.sqrt((X_train ** 2).sum(axis=1, keepdims=True)) / scale
    X_train_normalizer[X_train_normalizer < min_divisor] = 1
    
    X_test_normalizer = np.sqrt((X_test ** 2).sum(axis=1, keepdims=True)) / scale
    X_test_normalizer[X_test_normalizer < min_divisor] = 1

    X_train /= X_train_normalizer
    X_test /= X_test_normalizer
 
    if not X_valid==None:
        X_valid_mean = X_valid.mean(axis=1, keepdims=True)
        X_valid = X_valid - X_valid_mean
        X_valid_normalizer = np.sqrt((X_valid ** 2).sum(axis=1, keepdims=True)) / scale
        X_valid_normalizer[X_valid_normalizer < min_divisor] = 1
        X_valid /= X_valid_normalizer
        
    preprocessor = preprocessing.ZCA(store_inverse=True)
    preprocessor.fit(X_train)

    X_train_ZCA = preprocessor.transform(X_train)
    X_test_ZCA = preprocessor.transform(X_test)
    
    if not X_valid == None:
        X_valid_ZCA = preprocessor.transform(X_valid)

    to_dump = {}
    to_dump["train_mean"]=X_train_mean
    to_dump["test_mean"]=X_test_mean
    to_dump["scale"]=scale

    to_dump["train_normalizer"]= X_train_normalizer
    to_dump["test_normalizer"]= X_test_normalizer
    to_dump["preprocessor"]=preprocessor

    if not X_valid == None:
        to_dump["valid_mean"]=X_valid_mean
        to_dump["valid_normalizer"]= X_valid_normalizer

    if not os.path.exists(os.path.join(save_path, "preprocessor"+tag+".pkl")):
        pickle.dump(to_dump, open(os.path.join(save_path, "preprocessor"+tag+".pkl"), "w"))
    if X_valid == None:
        return X_train_ZCA, X_test_ZCA
    else:
        return X_train_ZCA, X_test_ZCA, X_valid_ZCA


def backward_process(X_test, save_path,X_train=None, tag=""):
    X_test = X_test.reshape([X_test.shape[0],np.prod(X_test.shape[1:4])])
    from_dump = pickle.load(open(os.path.join(save_path, "preprocessor"+tag+".pkl"), "r"))
    preprocessor = from_dump["preprocessor"]
    X_train_normalizer = from_dump["train_normalizer"]
    X_test_normalizer = from_dump["test_normalizer"]
    X_train_mean = from_dump["train_mean"]
    X_test_mean = from_dump["test_mean"]
    scale = float(from_dump["scale"])

    X_test = preprocessor.inverse(X_test)
    X_test = X_test * X_test_normalizer + X_test_mean

    if not X_train is None:
        X_train = preprocessor.inverse(X_train)
        X_train = X_train * X_train_normalizer + X_train_mean

    return X_test, X_train
