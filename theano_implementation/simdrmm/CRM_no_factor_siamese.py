__author__ = 'minhtannguyen'

import matplotlib as mpl

mpl.use('Agg')

import numpy as np

np.set_printoptions(threshold='nan')

from theano.tensor.nnet import conv2d
from theano.tensor.signal import pool

import numpy
import theano
import theano.tensor as T

from nn_functions_siamese import BatchNormalization
from theano.tensor.shared_randomstreams import RandomStreams

class CRM(object):
    """

    calling arguments:

    [TBA]

    internal variables:
    `data_4D_lab`:  labeled images
    `data_4D_unl`:  unlabeld images
    `data_4D_unl_clean`: unlabeled images used for the clean pass without noise. This is used when we add noise to the
                            the model to make it robust. We are not doing so now.
    `noise_weight`: control how much noise we want to add to the model
    `noise_std`: the standard deviation of the noise
    `K`:           Number of components (lambdas: rendering matrices)
    `M`:           Latent dimensionality
    `W`:           Width of the input
    `H`:           Height of the input
    `w`:           Width of the rendering matrices
    `h`:           Height of the rendering matrices
    `Cin`:         Number of channels of the input = Number of channels of the rendering matrices
    `Ni`:          Number of input images
    `momentum_bn`: control how to update the batch mean and var in batch normalization which will be used in testing
    `is_train`:     ={0,1} where 0: testing mode and 1: training mode
    `lambdas_val_init`: Initial values for rendering matrices
    `amps_val_init`: Initial values for priors
    `pool_t_mode`: Pooling mode={'max_t','mean_t`,None}
    `border_mode`: {'valid`, 'full', `half`}
    `pool_a_mode`: {`relu`, None}
    `nonlin`: {`relu`, `abs`}
    `mean_pool_size`: size of the mean pooling layer before the softmax regression
    `max_condition_number`: a condition number to make computations more stable. Set to 1.e3
    `xavier_init`: {True, False} use Xavier initialization or not
    `is_noisy`: {True, False} add noise to the model or not
    `is_bn_BU`: {True, False} do batch normalization in the bottom-up E step or not
    `is_bn_TD`: {True, False} do batch normalization in the top-down E step or not
    `epsilon`: used to avoid divide by 0
    `momentum_pi_t`: used to update the pi_t during training (Exponential Moving Average (EMA) update)
    `momentum_pi_a`: used to update the pi_a during training (EMA update)
    `is_tied_bn`: {True, False} use the same batch mean and variance for batch normalization in Bottom-Up and Top-Down
                    E step
    `is_Dg`: {True, False} apply diagonal matrix multiplication instead of batch normalization (this is an alternative for batch
                normalization that we developed. (Still in development, a.k.a. it has not worked as we want yet)
    `is_prun`: {True, False} apply synaptic/neuron pruning or not (Stil in development)
    """

    def __init__(self, data_4D_lab,
                 data_4D_unl_1,
                 data_4D_unl_2,
                 data_4D_unl_clean,
                 noise_weight,
                 noise_std,
                 K, M, W, H, w, h, Cin, Ni,
                 momentum_bn, is_train,
                 lambdas_val_init=None, amps_val_init=None,
                 gamma_val_init=None, beta_val_init=None,
                 prun_mat_init=None, prun_synap_mat_init=None,
                 mean_bn_init=None, var_bn_init=None,
                 pool_t_mode='max_t', border_mode='valid', pool_a_mode='relu', nonlin='relu',
                 mean_pool_size=(2, 2),
                 max_condition_number=1.e3,
                 xavier_init=False,
                 is_noisy=False,
                 is_bn_BU=False,
                 epsilon=1e-10,
                 momentum_pi_t=0.99,
                 momentum_pi_a=0.99,
                 momentum_pi_ta=0.99,
                 momentum_pi_synap=0.99,
                 is_prun=False,
                 is_prun_synap=False,
                 is_dn=False,
                 sigma_dn_init=None,
                 b_dn_init=None,
                 alpha_dn_init=None,
                 update_mean_var_with_sup=False):

        # required
        self.K = K  # number of filters
        self.M = M  # latent dimensionality
        self.data_4D_lab = data_4D_lab # labeled data
        self.data_4D_unl_1 = data_4D_unl_1 # unlabeled data 1
        self.data_4D_unl_2 = data_4D_unl_2 # unlabeled data 2
        self.data_4D_unl_clean = data_4D_unl_clean # unlabeled data used for the clean path
        self.noise_weight = noise_weight
        self.noise_std = noise_std
        self.Ni = Ni  # no. of labeled examples = no. of unlabeled examples
        self.w = w  # width of filters
        self.h = h  # height of filters
        self.Cin = Cin  # number of channels in the image
        self.D = self.h * self.w * self.Cin  # patch size
        self.W = W  # width of image
        self.H = H  # height of image
        if border_mode == 'valid':
            self.Np = (self.H - self.h + 1) * (self.W - self.w + 1)  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1)
        elif border_mode == 'half':
            self.Np = self.H * self.W  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H, self.W)
        elif border_mode == 'full':
            self.Np = (self.H + self.h - 1) * (self.W + self.w - 1)  # no. of patches per image
            self.latents_shape = (self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1)
        else:
            raise

        self.N = self.Ni * self.Np  # total no. of patches and total no. of hidden units
        self.mean_pool_size = mean_pool_size

        # self.means_val_init = means_val_init
        self.lambdas_val_init = lambdas_val_init
        self.amps_val_init = amps_val_init
        self.gamma_val_init = gamma_val_init
        self.beta_val_init = beta_val_init
        self.prun_synap_mat_init = prun_synap_mat_init
        self.prun_mat_init = prun_mat_init
        self.sigma_dn_init = sigma_dn_init
        self.mean_bn_init = mean_bn_init
        self.var_bn_init = var_bn_init
        self.b_dn_init = b_dn_init
        self.alpha_dn_init = alpha_dn_init

        # options
        self.pool_t_mode = pool_t_mode
        self.pool_a_mode = pool_a_mode
        self.nonlin = nonlin
        self.border_mode = border_mode
        self.max_condition_number = max_condition_number
        self.xavier_init = xavier_init
        self.is_noisy = is_noisy
        self.is_bn_BU = is_bn_BU
        self.momentum_bn = momentum_bn
        self.momentum_pi_t = momentum_pi_t
        self.momentum_pi_a = momentum_pi_a
        self.momentum_pi_ta = momentum_pi_ta
        self.momentum_pi_synap = momentum_pi_synap
        self.is_train = is_train
        self.epsilon = epsilon
        self.is_prun = is_prun
        self.is_prun_synap = is_prun_synap
        self.is_dn = is_dn
        self.update_mean_var_with_sup = update_mean_var_with_sup

        self._initialize()

    def _initialize(self):
        #
        # initialize pi's, means, lambdas, psis, lambda_covs, covs, and inv_covs
        #

        # initialize the pi's (a.k.a the priors)
        # if initial values for pi's are not provided, randomly initialize pi's

        # set up a random number generator
        self.srng = RandomStreams()
        self.srng.seed(np.random.randint(2 ** 15))

        # add noise to the input if is_noisy
        if self.is_noisy:
            if self.data_4D_unl_1 is not None:
                self.data_4D_unl_1 = self.data_4D_unl_1 + \
                                   self.noise_weight * self.srng.normal(size=(self.Ni, self.Cin, self.H, self.W), avg=0.0, std=self.noise_std)
            if self.data_4D_unl_2 is not None:
                self.data_4D_unl_2 = self.data_4D_unl_2 + \
                                   self.noise_weight * self.srng.normal(size=(self.Ni, self.Cin, self.H, self.W), avg=0.0, std=self.noise_std)
            if self.data_4D_lab is not None:
                self.data_4D_lab = self.data_4D_lab + \
                                   self.noise_weight * self.srng.normal(size=(self.Ni, self.Cin, self.H, self.W),
                                                                        avg=0.0, std=self.noise_std)

        # initialize amps
        # amps is class prior  e.g. cat, dog probabilities
        if self.amps_val_init is None:
            amps_val = np.random.rand(self.K)
            amps_val /= np.sum(amps_val)

        else:
            amps_val = self.amps_val_init

        self.amps = theano.shared(value=np.asarray(amps_val, dtype=theano.config.floatX),
                                  name='amps', borrow=True)

        # initialize t and a priors
        self.pi_t = theano.shared(value=numpy.ones(self.latents_shape[1:],
                                                     dtype=theano.config.floatX),name='pi_t', borrow=True)

        self.pi_a = theano.shared(value=numpy.ones(self.latents_shape[1:],
                                                    dtype=theano.config.floatX), name='pi_a', borrow=True)

        self.pi_a_old = theano.shared(value=numpy.ones(self.latents_shape[1:],
                                                   dtype=theano.config.floatX), name='pi_a_old', borrow=True)

        self.pi_ta = theano.shared(value=numpy.ones(self.latents_shape[1:],
                                                   dtype=theano.config.floatX), name='pi_ta', borrow=True)

        if self.prun_mat_init is None:
            self.prun_mat = theano.shared(value=numpy.ones(self.latents_shape[1:],
                                          dtype=theano.config.floatX), name='prun_mat', borrow=True)
        else:
            self.prun_mat = theano.shared(value=np.asarray(self.prun_mat_init, dtype=theano.config.floatX),
                                  name='prun_mat', borrow=True)

        if self.prun_synap_mat_init is None:
            self.prun_synap_mat = theano.shared(value=numpy.ones((self.K, self.Cin, self.h, self.w), dtype=theano.config.floatX),
                                                name='prun_synap_mat', borrow=True)
        else:
            self.prun_synap_mat = theano.shared(value=np.asarray(self.prun_synap_mat_init, dtype=theano.config.floatX),
                                          name='prun_mat', borrow=True)

        if self.is_prun_synap:
            self.pi_synap = theano.shared(
                value=numpy.ones((self.K, self.Cin, self.h, self.w), dtype=theano.config.floatX),
                name='pi_synap', borrow=True)

            self.pi_synap_old = theano.shared(
                value=numpy.ones((self.K, self.Cin, self.h, self.w), dtype=theano.config.floatX),
                name='pi_synap_old', borrow=True)

        # pi_t_final and pi_a_final are used after training for sampling
        self.pi_t_final = theano.shared(value=numpy.ones(self.latents_shape[1:],
                                                    dtype=theano.config.floatX), name='pi_t_final', borrow=True)

        self.pi_a_final = theano.shared(value=numpy.ones(self.latents_shape[1:],
                                                    dtype=theano.config.floatX), name='pi_a_final', borrow=True)

        # initialize the lambdas
        # if initial values for lambdas are not provided, randomly initialize lambdas
        if self.lambdas_val_init is None:
            if self.xavier_init:
                fan_in = self.D
                if self.pool_t_mode is None:
                    fan_out = self.K * self.h * self.w
                else:
                    fan_out = self.K * self.h * self.w / 4.

                lambdas_bound = np.sqrt(6. / (fan_in + fan_out))

                filter_dim = (self.K, self.D, self.M)

                lambdas_value = np.random.uniform(low=-lambdas_bound, high=lambdas_bound,
                                                  size=filter_dim)
            else:
                lambdas_value = np.random.randn(self.K, self.D, self.M) / \
                                np.sqrt(self.max_condition_number)
        else:
            lambdas_value = self.lambdas_val_init

        self.lambdas = theano.shared(value=np.asarray(lambdas_value, dtype=theano.config.floatX), name='lambdas',
                                     borrow=True)

        # Initialize BatchNorm
        if self.is_bn_BU:
            self.bn_BU = BatchNormalization(insize=self.K, mode=1, momentum=self.momentum_bn, is_train=self.is_train,
                                            epsilon=self.epsilon,
                                            gamma_val_init=self.gamma_val_init, beta_val_init=self.beta_val_init,
                                            mean_init=self.mean_bn_init, var_init=self.var_bn_init)

            self.params = [self.lambdas, self.bn_BU.gamma, self.bn_BU.beta]
        else:
            self.params = [self.lambdas, ]

        # Initialize the output
        self.output_lab = None
        self.output_clean = None
        self.output_1 = None
        self.output_2 = None

        if self.sigma_dn_init is None:
            self.sigma_dn = theano.shared(0.5*np.ones((1,), dtype=theano.config.floatX), name='sigma_dn', borrow=True)
        else:
            self.sigma_dn = theano.shared(value=np.asarray(self.sigma_dn_init, dtype=theano.config.floatX), name='sigma_dn',
                                 borrow=True)

        if self.b_dn_init is None:
            self.b_dn = theano.shared(0. * np.ones((1,), dtype=theano.config.floatX), name='b_dn', borrow=True)
        else:
            self.b_dn = theano.shared(value=np.asarray(self.b_dn_init, dtype=theano.config.floatX), name='b_dn',
                                 borrow=True)

        if self.alpha_dn_init is None:
            self.alpha_dn = theano.shared(np.ones((1,), dtype=theano.config.floatX), name='alpha_dn', borrow=True)
        else:
            self.alpha_dn = theano.shared(value=np.asarray(self.alpha_dn_init, dtype=theano.config.floatX), name='alpha_dn',
                                      borrow=True)

        if self.is_dn:
            self.params.append(self.sigma_dn)
            self.params.append(self.alpha_dn)
            self.params.append(self.b_dn)

    def get_important_latents_BU(self, input, betas):
        ##################################################################################
        # This function is used in the _E_step_Bottom_Up to compute latent representations
        ##################################################################################
        # compute E[z|x] using eq. 13
        latents_before_BN = conv2d(
            input=input,
            filters=betas,
            filter_shape=(self.K, self.Cin, self.h, self.w),
            input_shape=(self.Ni, self.Cin, self.H, self.W),
            filter_flip=False,
            border_mode=self.border_mode
        )

        # do Batch normalization
        if self.is_bn_BU:
            latents_after_BN = self.bn_BU.get_result(input=latents_before_BN, input_shape=self.latents_shape)
            scale_s = T.ones_like(latents_before_BN)
            latents_demeaned = latents_before_BN
            latents_demeaned_squared = latents_demeaned ** 2
        elif self.is_dn:
            filter_for_norm_local = theano.shared(value=np.ones((1, self.K, self.h, self.w), dtype=theano.config.floatX),
                                                  name='filter_norm_local', borrow=True)

            sum_local = conv2d(
                input=latents_before_BN,
                filters=filter_for_norm_local,
                filter_shape=(1, self.K, self.h, self.w),
                input_shape=self.latents_shape,
                filter_flip=False,
                border_mode='half'
            )

            mean_local = sum_local/(self.K*self.h*self.w)

            latents_demeaned = latents_before_BN - T.repeat(mean_local, self.K,  axis=1)

            latents_demeaned_squared = latents_demeaned**2

            norm_local = conv2d(
                input=latents_demeaned_squared,
                filters=filter_for_norm_local,
                filter_shape=(1, self.K, self.h, self.w),
                input_shape=self.latents_shape,
                filter_flip=False,
                border_mode='half'
            )

            scale_s = (T.repeat((self.alpha_dn + 1e-10), self.K).dimshuffle('x', 0, 'x', 'x')
                       + T.repeat(norm_local / (self.K * self.h * self.w), self.K, axis=1) / (
                       (T.repeat((self.sigma_dn + 1e-5), self.K).dimshuffle('x', 0, 'x', 'x')) ** 2)) / 2.

            latents_after_BN = (latents_demeaned / T.sqrt(scale_s)) + T.repeat(self.b_dn, self.K).dimshuffle('x', 0, 'x', 'x')

        else:
            latents_after_BN = latents_before_BN
            scale_s = T.ones_like(latents_before_BN)
            latents_demeaned = latents_before_BN
            latents_demeaned_squared = latents_demeaned ** 2

        latents = latents_after_BN * self.prun_mat

        mask_input = T.cast(T.gt(input, 0.), theano.config.floatX)

        # max over a
        if self.pool_a_mode == 'relu':
            max_over_a_mask = T.cast(T.gt(latents, 0.), theano.config.floatX)
        else:
            max_over_a_mask = T.cast(T.ones_like(latents), theano.config.floatX)

        # max over t
        if self.pool_t_mode == 'max_t' and self.nonlin == 'relu':
            max_over_t_mask = T.grad(
                T.sum(pool.pool_2d(input=latents, ds=(2, 2), ignore_border=True, mode='max')),
                wrt=latents)  # argmax across t
            max_over_t_mask = T.cast(max_over_t_mask, theano.config.floatX)
        elif self.pool_t_mode == 'max_t' and self.nonlin == 'abs': # still in the beta state
            latents_abs = T.abs_(latents)
            max_over_t_mask = T.grad(
                T.sum(pool.pool_2d(input=latents_abs, ds=(2, 2), ignore_border=True, mode='max')),
                wrt=latents_abs)  # argmax across t
            max_over_t_mask = T.cast(max_over_t_mask, theano.config.floatX)
        else:
            # compute latents masked by a
            max_over_t_mask = T.cast(T.ones_like(latents), theano.config.floatX)

        # compute latents masked by a and t
        if self.nonlin == 'relu':
            latents_masked = T.nnet.relu(latents, alpha=0.) * max_over_t_mask  # * max_over_a_mask
        elif self.nonlin == 'abs':
            latents_masked = T.abs_(latents) * max_over_t_mask
        else:
            raise

        masked_mat = max_over_t_mask * max_over_a_mask  # * max_over_a_mask

        if self.pool_t_mode == 'max_t':
            output = pool.pool_2d(input=latents_masked, ds=(2, 2),
                                  ignore_border=True, mode='average_exc_pad')
            output = output * 4.0
        elif self.pool_t_mode == 'mean_t':
            output = pool.pool_2d(input=latents_masked, ds=self.mean_pool_size,
                                  ignore_border=True, mode='average_exc_pad')
        else:
            output = latents_masked

        return latents_before_BN, latents, max_over_a_mask, max_over_t_mask, latents_masked, masked_mat, output, mask_input, scale_s, latents_demeaned, latents_demeaned_squared


    def _E_step_Bottom_Up(self):
        """
        Expectation step through all clusters.
        Compute responsibilities, likelihoods, lambda dagger, latents, latent_covs, pi's
        """

        # Bottom-Up

        # compute the betas
        self.betas = self.lambdas.dimshuffle(0, 2, 1)
        betas = T.reshape(self.betas[:, 0, :], newshape=(self.K, self.Cin, self.h, self.w))
        betas = betas * self.prun_synap_mat


        # run bottom up for labeled examples.
        if self.is_bn_BU:
            if self.data_4D_lab is not None and self.data_4D_unl_clean is None: # supervised training mode
                self.bn_BU.set_runmode(0)
            elif self.data_4D_lab is not None and self.data_4D_unl_clean is None: # semisupervised training mode
                if self.update_mean_var_with_sup:
                    self.bn_BU.set_runmode(0)
                else:
                    self.bn_BU.set_runmode(1)

        [self.latents_before_BN_lab, self.latents_lab, self.max_over_a_mask_lab,
         self.max_over_t_mask_lab, self.latents_masked_lab, self.masked_mat_lab, self.output_lab, self.mask_input_lab, self.scale_s_lab,
         self.latents_demeaned_lab, self.latents_demeaned_squared_lab] \
            = self.get_important_latents_BU(input=self.data_4D_lab, betas=betas)

        # run bottom up for unlabeled data when noise is added
        if self.is_noisy:
            if self.data_4D_unl_1 is not None:
                if self.is_bn_BU:
                    self.bn_BU.set_runmode(1)  # no update of means and vars
                [self.latents_before_BN_1, self.latents_1, self.max_over_a_mask_1, self.max_over_t_mask_1, self.latents_masked_1, self.masked_mat_1,
                 self.output_1, self.mask_input_1, self.scale_s_1, self.latents_demeaned_1, self.latents_demeaned_squared_1] \
                    = self.get_important_latents_BU(input=self.data_4D_unl_1, betas=betas)

            if self.data_4D_unl_2 is not None:
                if self.is_bn_BU:
                    self.bn_BU.set_runmode(1)  # no update of means and vars
                [self.latents_before_BN_2, self.latents_2, self.max_over_a_mask_2, self.max_over_t_mask_2, self.latents_masked_2, self.masked_mat_2,
                 self.output_2, self.mask_input_2, self.scale_s_2, self.latents_demeaned_2, self.latents_demeaned_squared_2] \
                    = self.get_important_latents_BU(input=self.data_4D_unl_2, betas=betas)

            if self.data_4D_unl_clean is not None:
                if self.is_bn_BU:
                    self.bn_BU.set_runmode(0)  # using clean data to keep track the means and the vars
                [self.latents_before_BN_clean, self.latents_clean, self.max_over_a_mask_clean, self.max_over_t_mask_clean, self.latents_masked_clean,
                 self.masked_mat_clean, self.output_clean, self.mask_input_clean, self.scale_s_clean,
                 self.latents_demeaned_clean, self.latents_demeaned_squared_clean] \
                    = self.get_important_latents_BU(input=self.data_4D_unl_clean, betas=betas)

        else:
            if self.data_4D_unl_1 is not None:
                if self.is_bn_BU:
                    if self.update_mean_var_with_sup:
                        self.bn_BU.set_runmode(1) # using clean data to keep track the means and vars
                    else:
                        self.bn_BU.set_runmode(0)  # using clean data to keep track the means and vars

                [self.latents_before_BN_1, self.latents_1, self.max_over_a_mask_1, self.max_over_t_mask_1, self.latents_masked_1, self.masked_mat_1,
                 self.output_1, self.mask_input_1, self.scale_s_1, self.latents_demeaned_1, self.latents_demeaned_squared_1] \
                    = self.get_important_latents_BU(input=self.data_4D_unl_1, betas=betas)
                self.output_clean = self.output_1

            if self.data_4D_unl_2 is not None:
                if self.is_bn_BU:
                    self.bn_BU.set_runmode(1)  # using clean data to keep track the means and vars

                [self.latents_before_BN_2, self.latents_2, self.max_over_a_mask_2, self.max_over_t_mask_2, self.latents_masked_2, self.masked_mat_2,
                 self.output_2, self.mask_input_2, self.scale_s_2, self.latents_demeaned_2, self.latents_demeaned_squared_2] \
                    = self.get_important_latents_BU(input=self.data_4D_unl_2, betas=betas)


        if self.data_4D_unl_1 is not None:
            self.pi_t_minibatch = T.mean(self.max_over_t_mask_1, axis=0)
            self.pi_a_minibatch = T.mean(self.max_over_a_mask_1, axis=0)
            self.pi_ta_minibatch = T.mean(self.masked_mat_1, axis=0)
            self.pi_t_new = self.momentum_pi_t * self.pi_t + (1. - self.momentum_pi_t) * self.pi_t_minibatch
            self.pi_a_new = self.momentum_pi_a * self.pi_a + (1 - self.momentum_pi_a)*self.pi_a_minibatch
            self.pi_ta_new = self.momentum_pi_ta * self.pi_ta + (1. - self.momentum_pi_ta) * self.pi_ta_minibatch

            if self.is_prun_synap:
                padded_mask_input, padded_shape = self.pad_images(images=self.mask_input_1.dimshuffle(1, 0, 2, 3),
                                                                  image_shape=(self.Cin, self.Ni, self.H, self.W),
                                                                  filter_size=(self.h, self.w),
                                                                  border_mode=self.border_mode)

                pi_synap_minibatch = conv2d(input=padded_mask_input,
                                            filters=self.max_over_a_mask_1.dimshuffle(1, 0, 2, 3),
                                            filter_shape=(self.K, self.Ni, self.latents_shape[2], self.latents_shape[3]),
                                            image_shape=padded_shape,
                                            filter_flip=False,
                                            border_mode='valid')

                self.pi_synap_minibatch = T.cast(T.unbroadcast(pi_synap_minibatch, 0).dimshuffle(1, 0, 2, 3)
                                                         /np.float32(self.Ni*self.latents_shape[2]*self.latents_shape[3]), theano.config.floatX)

            self.amps_new = T.sum(self.masked_mat_1, axis=(0, 2, 3)) / np.float32(self.N / 4.0)

        else: # if supervised learning
            self.pi_t_minibatch = T.mean(self.max_over_t_mask_lab, axis=0)
            self.pi_a_minibatch = T.mean(self.max_over_a_mask_lab, axis=0)
            self.pi_ta_minibatch = T.mean(self.masked_mat_lab, axis=0)
            self.pi_t_new = self.momentum_pi_t * self.pi_t + (1 - self.momentum_pi_t) * self.pi_t_minibatch
            self.pi_a_new = self.momentum_pi_a * self.pi_a + (1 - self.momentum_pi_a) * self.pi_a_minibatch
            self.pi_ta_new = self.momentum_pi_ta * self.pi_ta + (1 - self.momentum_pi_ta) * self.pi_ta_minibatch

            if self.is_prun_synap:
                padded_mask_input, padded_shape = self.pad_images(images=self.mask_input_lab.dimshuffle(1, 0, 2, 3),
                                                                  image_shape=(self.Cin, self.Ni, self.H, self.W),
                                                                  filter_size=(self.h, self.w),
                                                                  border_mode=self.border_mode)

                pi_synap_minibatch = conv2d(input=padded_mask_input,
                                            filters=self.max_over_a_mask_lab.dimshuffle(1, 0, 2, 3),
                                            filter_shape=(self.K, self.Ni, self.latents_shape[2], self.latents_shape[3]),
                                            input_shape=padded_shape,
                                            filter_flip=False,
                                            border_mode='valid')

                self.pi_synap_minibatch = T.cast(T.unbroadcast(pi_synap_minibatch, 0).dimshuffle(1, 0, 2, 3)
                                           / np.float32(self.Ni * self.latents_shape[2] * self.latents_shape[3]),
                                           theano.config.floatX)

            self.amps_new = T.sum(self.masked_mat_lab, axis=(0, 2, 3)) / np.float32(self.N / 4.0)

    def _E_step_Top_Down_Reconstruction(self, mu_cg_1, mu_cg_2, mu_cg_lab):
        # Reconstruct/Sample the images to compute the complete-data log-likelihood
        # input is of size Ni x K x (H-h+1)/2 x (W-w+1)/2
        #

        # Up-sample the latent presentations
        if self.pool_t_mode == 'max_t':
            self.latents_unpooled_no_mask_1 = mu_cg_1.repeat(2, axis=2).repeat(2, axis=3)
            self.latents_unpooled_no_mask_2 = mu_cg_2.repeat(2, axis=2).repeat(2, axis=3)
            self.latents_unpooled_no_mask_lab = mu_cg_lab.repeat(2, axis=2).repeat(2, axis=3)
        elif self.pool_t_mode == 'mean_t':
            self.latents_unpooled_no_mask_1 = mu_cg_1.repeat(self.mean_pool_size[0], axis=2).repeat(self.mean_pool_size[1], axis=3)
            self.latents_unpooled_no_mask_2 = mu_cg_2.repeat(self.mean_pool_size[0], axis=2).repeat(
                self.mean_pool_size[1], axis=3)
            self.latents_unpooled_no_mask_lab = mu_cg_lab.repeat(self.mean_pool_size[0], axis=2).repeat(
                self.mean_pool_size[1],axis=3)
        elif self.pool_t_mode is None:
            self.latents_unpooled_no_mask_1 = mu_cg_1
            self.latents_unpooled_no_mask_2 = mu_cg_2
            self.latents_unpooled_no_mask_lab = mu_cg_lab
        else:
            raise

        self.latents_unpooled_1 = self.latents_unpooled_no_mask_1 * self.masked_mat_1 * self.prun_mat
        self.latents_unpooled_2 = self.latents_unpooled_no_mask_2 * self.masked_mat_2 * self.prun_mat
        self.latents_unpooled_lab = self.latents_unpooled_no_mask_lab * self.masked_mat_lab * self.prun_mat

        # reconstruct/sample the image
        self.lambdas_deconv = (T.reshape(self.lambdas[:, :, 0],
                                         newshape=(self.K, self.Cin, self.h, self.w)) * self.prun_synap_mat).dimshuffle(1, 0, 2, 3)
        self.lambdas_deconv = self.lambdas_deconv[:, :, ::-1, ::-1]

        if self.border_mode == 'valid':
            self.data_reconstructed_1 = conv2d(
                input=self.latents_unpooled_1,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                input_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
                filter_flip=False,
                border_mode='full'
            )
            self.data_reconstructed_2 = conv2d(
                input=self.latents_unpooled_2,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                input_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
                filter_flip=False,
                border_mode='full'
            )
            self.data_reconstructed_lab = conv2d(
                input=self.latents_unpooled_lab,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                input_shape=(self.Ni, self.K, self.H - self.h + 1, self.W - self.w + 1),
                filter_flip=False,
                border_mode='full'
            )
        elif self.border_mode == 'half':
            self.data_reconstructed_1 = conv2d(
                input=self.latents_unpooled_1,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                input_shape=(self.Ni, self.K, self.H, self.W),
                filter_flip=False,
                border_mode='half'
            )
            self.data_reconstructed_2 = conv2d(
                input=self.latents_unpooled_2,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                input_shape=(self.Ni, self.K, self.H, self.W),
                filter_flip=False,
                border_mode='half'
            )
            self.data_reconstructed_lab = conv2d(
                input=self.latents_unpooled_lab,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                input_shape=(self.Ni, self.K, self.H, self.W),
                filter_flip=False,
                border_mode='half'
            )
        else:
            self.data_reconstructed_1 = conv2d(
                input=self.latents_unpooled_1,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                input_shape=(self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1),
                filter_flip=False,
                border_mode='valid'
            )
            self.data_reconstructed_2 = conv2d(
                input=self.latents_unpooled_2,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                input_shape=(self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1),
                filter_flip=False,
                border_mode='valid'
            )
            self.data_reconstructed_lab = conv2d(
                input=self.latents_unpooled_lab,
                filters=self.lambdas_deconv,
                filter_shape=(self.Cin, self.K, self.h, self.w),
                input_shape=(self.Ni, self.K, self.H + self.h - 1, self.W + self.w - 1),
                filter_flip=False,
                border_mode='valid'
            )

        # compute reconstruction error
        self.reconstruction_error = (T.mean((self.data_4D_unl_1 - self.data_reconstructed_1) ** 2) \
                                    + T.mean((self.data_4D_unl_2 - self.data_reconstructed_2) ** 2))/2.

        self.reconstruction_error_lab = T.mean((self.data_4D_lab - self.data_reconstructed_lab) ** 2)

    def pad_images(self, images, image_shape, filter_size, border_mode):
        """
        pad image with the given pad_size
        """
        # Allocate space for padded images.
        if border_mode == 'valid':
            x_padded = images
            padded_shape = image_shape
        else:
            if border_mode == 'half':
                h_pad = filter_size[0] // 2
                w_pad = filter_size[1] // 2
            elif border_mode == 'full':
                h_pad = filter_size[0] - 1
                w_pad = filter_size[1] - 1

            s = image_shape
            padded_shape = (s[0], s[1], s[2] + 2*h_pad, s[3] + 2*w_pad)
            x_padded = T.zeros(padded_shape)

            # Copy the original image to the central part.
            x_padded = T.set_subtensor(
                x_padded[:, :, h_pad:s[2]+h_pad, w_pad:s[3]+w_pad],
                images,
            )

        return x_padded, padded_shape