import cPickle as pickle
import time

import numpy as np
import theano
from theano import tensor as T

from CRM_no_factor_siamese import CRM
from DRM_no_factor_siamese import DRM_model
from nn_functions_siamese import SoftmaxNonlinearity, \
    HiddenLayerInSoftmax, HiddenLayerInSoftmaxSiamese, SoftmaxNonlinearitySiamese

########################################################################################################################
class SHAPENET_Conv_Large_9_Layers(DRM_model):
    '''
    Conv_Small that the Ladder Network uses for MNIST

    `batch_size`:   size of minibatches used in each iteration
    `Cin`: The number of channels of the input
    `W`: The width of the input
    `H`: The height of the input
    `seed`: random seed
    `param_dir`: dir that saved the trained DRMM. Used in transfer learning or just to continue the training that stopped
                 for some reason
    `train_mode`: supervised, semisupervised, unsupervised
    `reconst_weights`: used to weigh reconstruction cost at each layer in the DRMM
    `xavier_init`: {True, False} use Xavier initialization or not
    `grad_min, grad_max`: clip the gradients
    `init_params`: {True, False} if True, load parameters from a trained DRMM
    `denoising`: {simple, message-passing} denoising mode
    `noise_std`: standard deviation of the noise added to the model
    `is_bn_BU`: {True, False} use batch normalization in the Bottom-Up or not
    `is_bn_TD`: {True, False} use batch normalization in the Top-Down or not
    `top_down_mode`: {signed, normal} use signed DRMM or NN-DRMM
    `is_TD_relu`: {True, False} use ReLU in the TopDown or not
    `nonlin`: {relu, abs} the nonlinearity to use in the Bottom-Up
    `is_tied_bn`: {True, False} if True, tie Batch Norm in the Bottom-Up and Top-Down
    `is_Dg`: {True, False} if True, use matrix preconditioning
    `method`: {SGD, adam} training method
    `KL_coef`: weight of the KL divergence cost
    `sign_cost_weight`: weight of hte sign cost
    `is_reluI`: {True, False} if True, apply ReLU on the reconstructed image
    `is_end_to_end`: {True, False} if True, train end-to-end
    `is_sample_c`: {True, False} if True, do sampling during reconstruction
    `is_one_hot`: {True, False} if True, turn c_hat into a one hot encoding vector and use it to reconstruct the image
    `num_TD_samplings`: number of samples used in the Top-Down reconstruction
    `KL_t_coef`: weight of the KL divergence on t cost. We are not using it now.
    `is_pruning`: do pruning or not
    `prun_threshold`: threshold used to prun the connections
    '''
    # TODO: factor out common init code from all models
    def __init__(self, batch_size, Cin, W, H, seed, param_dir=[],
                 reconst_weights=0.0, xavier_init=False, grad_min=-np.inf, grad_max=np.inf,
                 init_params=False, noise_std=0.45, noise_weights=[0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0],
                 is_bn_BU=False, nonlin='relu',
                 method='SGD', KL_coef=0.0, sign_cost_weight=0.0,
                 is_prun=False, is_prun_synap=False, is_prun_songhan=False, is_finetune_after_prun=False,
                 param_dir_for_prun_mat=[], is_dn=False,
                 update_mean_var_with_sup=False,
                 cost_mode='sup',
                 is_sup = True):

        self.num_class = 720

        self.noise_std = noise_std
        self.noise_weights = noise_weights
        self.reconst_weights = reconst_weights
        self.sign_cost_weight = sign_cost_weight
        self.is_bn_BU = is_bn_BU
        self.is_dn = is_dn
        self.batch_size = batch_size
        self.grad_min = grad_min
        self.grad_max = grad_max
        self.nonlin = nonlin
        self.method = method
        self.KL_coef = KL_coef
        self.is_prun = is_prun
        self.is_prun_synap = is_prun_synap
        self.is_prun_songhan=is_prun_songhan
        self.update_mean_var_with_sup = update_mean_var_with_sup
        self.cost_mode = cost_mode
        self.is_sup = is_sup

        is_noisy = [False] * len(self.noise_weights)
        for i in xrange(len(is_noisy)):
            if np.sum(self.noise_weights[0:i + 1]) > 0.0:
                is_noisy[i] = True

        # Initialize params
        if init_params:
            time.sleep(1)
            pkl_file = open(param_dir, 'rb')
            model_loaded = pickle.load(pkl_file)
            lambdas_val_init = [model_loaded.layers[l].lambdas.get_value() for l in range(len(self.noise_weights))]
            amps_val_init = [model_loaded.layers[l].amps.get_value() for l in range(len(self.noise_weights))]
            if self.is_bn_BU:
                gamma_val_init = [model_loaded.layers[l].bn_BU.gamma.get_value() for l in
                                  range(len(self.noise_weights))]
                beta_val_init = [model_loaded.layers[l].bn_BU.beta.get_value() for l in
                                 range(len(self.noise_weights))]
                mean_bn_init = [model_loaded.layers[l].bn_BU.mean.get_value() for l in
                                range(len(self.noise_weights))]
                var_bn_init = [model_loaded.layers[l].bn_BU.var.get_value() for l in range(len(self.noise_weights))]
            else:
                gamma_val_init = [None] * len(self.noise_weights)
                beta_val_init = [None] * len(self.noise_weights)
                mean_bn_init = [None] * len(self.noise_weights)
                var_bn_init = [None] * len(self.noise_weights)

            W_init = model_loaded.RegressionInSoftmax.W.get_value()
            b_init = model_loaded.RegressionInSoftmax.b.get_value()
            W_Siamese_init = model_loaded.RegressionSiamese.W.get_value()
            b_Siamese_init = model_loaded.RegressionSiamese.b.get_value()

            if self.is_dn:
                sigma_dn_init = [model_loaded.layers[l].sigma_dn.get_value() for l in
                                 range(len(self.noise_weights))]
                alpha_dn_init = [model_loaded.layers[l].alpha_dn.get_value() for l in
                                    range(len(self.noise_weights))]
                b_dn_init = [model_loaded.layers[l].b_dn.get_value() for l in range(len(self.noise_weights))]

            else:
                sigma_dn_init = [None] * len(self.noise_weights)
                b_dn_init = [None] * len(self.noise_weights)
                alpha_dn_init = [None] * len(self.noise_weights)

            pkl_file.close()
        else:
            lambdas_val_init = [None] * len(self.noise_weights)
            amps_val_init = [None] * len(self.noise_weights)
            gamma_val_init = [None] * len(self.noise_weights)
            beta_val_init = [None] * len(self.noise_weights)
            mean_bn_init = [None] * len(self.noise_weights)
            var_bn_init = [None] * len(self.noise_weights)
            sigma_dn_init = [None] * len(self.noise_weights)
            b_dn_init = [None] * len(self.noise_weights)
            alpha_dn_init = [None] * len(self.noise_weights)
            W_init = None
            b_init = None
            W_Siamese_init = None
            b_Siamese_init = None

        # Init for Finetuning after Pruning
        if is_finetune_after_prun:
            time.sleep(1)
            pkl_file = open(param_dir_for_prun_mat, 'rb')
            model_loaded = pickle.load(pkl_file)
            prun_mat_init = [model_loaded.layers[l].prun_mat.get_value() for l in range(len(self.noise_weights))]
            prun_synap_mat_init = [model_loaded.layers[l].prun_synap_mat.get_value() for l in
                                   range(len(self.noise_weights))]
            pkl_file.close()
        else:
            prun_mat_init = [None] * len(self.noise_weights)
            prun_synap_mat_init = [None] * len(self.noise_weights)

        # Build the model

        self.H1 = H  # 64
        self.W1 = W  # 64
        self.Cin1 = Cin  # 1

        self.h1 = 3
        self.w1 = 3
        self.K1 = 96
        self.M1 = 1

        self.H2 = self.H1 / 2  # 32
        self.W2 = self.W1 / 2  # 32
        self.Cin2 = self.K1  # 96

        self.h2 = 3
        self.w2 = 3
        self.K2 = 96
        self.M2 = 1

        self.H3 = self.H2 + self.h2 - 1  # 34
        self.W3 = self.W2 + self.w2 - 1  # 34
        self.Cin3 = self.K2  # 96

        self.h3 = 3
        self.w3 = 3
        self.K3 = 96
        self.M3 = 1

        self.H4 = (self.H3 + self.h3 - 1) / 2  # 18
        self.W4 = (self.W3 + self.w3 - 1) / 2  # 18
        self.Cin4 = self.K3  # 96

        self.h4 = 3
        self.w4 = 3
        self.K4 = 192
        self.M4 = 1

        self.H5 = self.H4 - self.h4 + 1  # 16
        self.W5 = self.W4 - self.w4 + 1  # 16
        self.Cin5 = self.K4  # 192

        self.h5 = 3
        self.w5 = 3
        self.K5 = 192
        self.M5 = 1

        self.H6 = self.H5 + self.h5 - 1  # 18
        self.W6 = self.W5 + self.w5 - 1  # 18
        self.Cin6 = self.K5  # 192

        self.h6 = 3
        self.w6 = 3
        self.K6 = 192
        self.M6 = 1

        self.H7 = (self.H6 - self.h6 + 1) / 2  # 8
        self.W7 = (self.W6 - self.w6 + 1) / 2  # 8
        self.Cin7 = self.K6  # 192

        self.h7 = 3
        self.w7 = 3
        self.K7 = 192
        self.M7 = 1

        self.H8 = self.H7 - self.h7 + 1  # 6
        self.W8 = self.W7 - self.w7 + 1  # 6
        self.Cin8 = self.K7  # 192

        self.h8 = 1
        self.w8 = 1
        self.K8 = 192
        self.M8 = 1

        self.H9 = self.H8 - self.h8 + 1  # 6
        self.W9 = self.W8 - self.w8 + 1  # 6
        self.Cin9 = self.K8  # 192

        self.h9 = 1
        self.w9 = 1
        self.K9 = self.num_class
        self.M9 = 1

        self.H_Softmax = 1
        self.W_Softmax = 1
        self.Cin_Softmax = self.K9  # self.num_class

        self.h_Softmax = 1
        self.w_Softmax = 1
        self.K_Softmax = self.num_class
        self.M_Softmax = 1

        self.seed = seed
        np.random.seed(self.seed)

        self.x_lab = T.tensor4('x_lab')
        self.x_unl_1 = T.tensor4('x_unl_1')
        self.x_unl_2 = T.tensor4('x_unl_2')

        self.y_lab = T.ivector('y_lab')
        self.y_siamese = T.ivector('y_siamese')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.prun_threshold = T.vector('prun_threshold', dtype=theano.config.floatX)
        self.prun_weights = T.vector('prun_weights', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

        # Forward Step
        self.conv1 = CRM(
            data_4D_lab=self.x_lab,
            data_4D_unl_1=self.x_unl_1,
            data_4D_unl_2=self.x_unl_2,
            data_4D_unl_clean=self.x_unl_1,
            noise_weight=self.noise_weights[0],
            noise_std=self.noise_std,
            is_train=self.is_train, momentum_bn=self.momentum_bn,
            K=self.K1, M=self.M1, W=self.W1, H=self.H1,
            w=self.w1, h=self.h1, Cin=self.Cin1, Ni=self.batch_size,
            amps_val_init=amps_val_init[8], lambdas_val_init=lambdas_val_init[8],
            gamma_val_init=gamma_val_init[8], beta_val_init=beta_val_init[8],
            prun_mat_init=prun_mat_init[8], prun_synap_mat_init=prun_synap_mat_init[8],
            mean_bn_init=mean_bn_init[8], var_bn_init=var_bn_init[8],
            sigma_dn_init=sigma_dn_init[8], b_dn_init=b_dn_init[8], alpha_dn_init=alpha_dn_init[8],
            pool_t_mode='max_t',
            border_mode='half',
            max_condition_number=1.e3, xavier_init=xavier_init,
            is_noisy=is_noisy[0], is_bn_BU=self.is_bn_BU,
            nonlin=self.nonlin,
            is_prun=self.is_prun,
            is_prun_synap=self.is_prun_synap,
            is_dn=self.is_dn,
            update_mean_var_with_sup=self.update_mean_var_with_sup)

        self.conv1._E_step_Bottom_Up()

        self.conv2 = CRM(data_4D_lab=self.conv1.output_lab,
                         data_4D_unl_1=self.conv1.output_1,
                         data_4D_unl_2=self.conv1.output_2,
                         data_4D_unl_clean=self.conv1.output_clean,
                         noise_weight=self.noise_weights[1],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K2, M=self.M2, W=self.W2, H=self.H2,
                         w=self.w2, h=self.h2, Cin=self.Cin2, Ni=self.batch_size,
                         amps_val_init=amps_val_init[7], lambdas_val_init=lambdas_val_init[7],
                         gamma_val_init=gamma_val_init[7], beta_val_init=beta_val_init[7],
                         prun_mat_init=prun_mat_init[7], prun_synap_mat_init=prun_synap_mat_init[7],
                         mean_bn_init=mean_bn_init[7], var_bn_init=var_bn_init[7],
                         sigma_dn_init=sigma_dn_init[7], b_dn_init=b_dn_init[7], alpha_dn_init=alpha_dn_init[7],
                         pool_t_mode=None,
                         border_mode='full',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[1], is_bn_BU=self.is_bn_BU,
                         nonlin=self.nonlin,
                         is_prun=self.is_prun,
                         is_prun_synap=self.is_prun_synap,
                         is_dn=self.is_dn,
                         update_mean_var_with_sup=self.update_mean_var_with_sup)

        self.conv2._E_step_Bottom_Up()

        self.conv3 = CRM(data_4D_lab=self.conv2.output_lab,
                         data_4D_unl_1=self.conv2.output_1,
                         data_4D_unl_2=self.conv2.output_2,
                         data_4D_unl_clean=self.conv2.output_clean,
                         noise_weight=self.noise_weights[2],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K3, M=self.M3, W=self.W3, H=self.H3,
                         w=self.w3, h=self.h3, Cin=self.Cin3, Ni=self.batch_size,
                         amps_val_init=amps_val_init[6], lambdas_val_init=lambdas_val_init[6],
                         gamma_val_init=gamma_val_init[6], beta_val_init=beta_val_init[6],
                         prun_mat_init=prun_mat_init[6], prun_synap_mat_init=prun_synap_mat_init[6],
                         mean_bn_init=mean_bn_init[6], var_bn_init=var_bn_init[6],
                         sigma_dn_init=sigma_dn_init[6], b_dn_init=b_dn_init[6], alpha_dn_init=alpha_dn_init[6],
                         pool_t_mode='max_t',
                         border_mode='full',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[2], is_bn_BU=self.is_bn_BU,
                         nonlin=self.nonlin,
                         is_prun=self.is_prun,
                         is_prun_synap=self.is_prun_synap,
                         is_dn=self.is_dn,
                         update_mean_var_with_sup=self.update_mean_var_with_sup)

        self.conv3._E_step_Bottom_Up()

        self.conv4 = CRM(data_4D_lab=self.conv3.output_lab,
                         data_4D_unl_1=self.conv3.output_1,
                         data_4D_unl_2=self.conv3.output_2,
                         data_4D_unl_clean=self.conv3.output_clean,
                         noise_weight=self.noise_weights[3],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K4, M=self.M4, W=self.W4, H=self.H4,
                         w=self.w4, h=self.h4, Cin=self.Cin4, Ni=self.batch_size,
                         amps_val_init=amps_val_init[5], lambdas_val_init=lambdas_val_init[5],
                         gamma_val_init=gamma_val_init[5], beta_val_init=beta_val_init[5],
                         prun_mat_init=prun_mat_init[5], prun_synap_mat_init=prun_synap_mat_init[5],
                         mean_bn_init=mean_bn_init[5], var_bn_init=var_bn_init[5],
                         sigma_dn_init=sigma_dn_init[5], b_dn_init=b_dn_init[5], alpha_dn_init=alpha_dn_init[5],
                         pool_t_mode=None,
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[3], is_bn_BU=self.is_bn_BU,
                         nonlin=self.nonlin,
                         is_prun=self.is_prun,
                         is_prun_synap=self.is_prun_synap,
                         is_dn=self.is_dn,
                         update_mean_var_with_sup=self.update_mean_var_with_sup)

        self.conv4._E_step_Bottom_Up()

        self.conv5 = CRM(data_4D_lab=self.conv4.output_lab,
                         data_4D_unl_1=self.conv4.output_1,
                         data_4D_unl_2=self.conv4.output_2,
                         data_4D_unl_clean=self.conv4.output_clean,
                         noise_weight=self.noise_weights[4],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K5, M=self.M5, W=self.W5, H=self.H5,
                         w=self.w5, h=self.h5, Cin=self.Cin5, Ni=self.batch_size,
                         amps_val_init=amps_val_init[4], lambdas_val_init=lambdas_val_init[4],
                         gamma_val_init=gamma_val_init[4], beta_val_init=beta_val_init[4],
                         prun_mat_init=prun_mat_init[4], prun_synap_mat_init=prun_synap_mat_init[4],
                         mean_bn_init=mean_bn_init[4], var_bn_init=var_bn_init[4],
                         sigma_dn_init=sigma_dn_init[4], b_dn_init=b_dn_init[4], alpha_dn_init=alpha_dn_init[4],
                         pool_t_mode=None,
                         border_mode='full',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[4], is_bn_BU=self.is_bn_BU,
                         nonlin=self.nonlin,
                         is_prun=self.is_prun,
                         is_prun_synap=self.is_prun_synap,
                         is_dn=self.is_dn,
                         update_mean_var_with_sup=self.update_mean_var_with_sup)

        self.conv5._E_step_Bottom_Up()

        self.conv6 = CRM(data_4D_lab=self.conv5.output_lab,
                         data_4D_unl_1=self.conv5.output_1,
                         data_4D_unl_2=self.conv5.output_2,
                         data_4D_unl_clean=self.conv5.output_clean,
                         noise_weight=self.noise_weights[5],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K6, M=self.M6, W=self.W6, H=self.H6,
                         w=self.w6, h=self.h6, Cin=self.Cin6, Ni=self.batch_size,
                         amps_val_init=amps_val_init[3], lambdas_val_init=lambdas_val_init[3],
                         gamma_val_init=gamma_val_init[3], beta_val_init=beta_val_init[3],
                         prun_mat_init=prun_mat_init[3], prun_synap_mat_init=prun_synap_mat_init[3],
                         mean_bn_init=mean_bn_init[3], var_bn_init=var_bn_init[3],
                         sigma_dn_init=sigma_dn_init[3], b_dn_init=b_dn_init[3], alpha_dn_init=alpha_dn_init[3],
                         pool_t_mode='max_t',
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[5], is_bn_BU=self.is_bn_BU,
                         nonlin=self.nonlin,
                         is_prun=self.is_prun,
                         is_prun_synap=self.is_prun_synap,
                         is_dn=self.is_dn,
                         update_mean_var_with_sup=self.update_mean_var_with_sup)

        self.conv6._E_step_Bottom_Up()

        self.conv7 = CRM(data_4D_lab=self.conv6.output_lab,
                         data_4D_unl_1=self.conv6.output_1,
                         data_4D_unl_2=self.conv6.output_2,
                         data_4D_unl_clean=self.conv6.output_clean,
                         noise_weight=self.noise_weights[6],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K7, M=self.M7, W=self.W7, H=self.H7,
                         w=self.w7, h=self.h7, Cin=self.Cin7, Ni=self.batch_size,
                         amps_val_init=amps_val_init[2], lambdas_val_init=lambdas_val_init[2],
                         gamma_val_init=gamma_val_init[2], beta_val_init=beta_val_init[2],
                         prun_mat_init=prun_mat_init[2], prun_synap_mat_init=prun_synap_mat_init[2],
                         mean_bn_init=mean_bn_init[2], var_bn_init=var_bn_init[2],
                         sigma_dn_init=sigma_dn_init[2], b_dn_init=b_dn_init[2], alpha_dn_init=alpha_dn_init[2],
                         pool_t_mode=None,
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[6], is_bn_BU=self.is_bn_BU,
                         nonlin=self.nonlin,
                         is_prun=self.is_prun,
                         is_prun_synap=self.is_prun_synap,
                         is_dn=self.is_dn,
                         update_mean_var_with_sup=self.update_mean_var_with_sup)

        self.conv7._E_step_Bottom_Up()

        self.conv8 = CRM(data_4D_lab=self.conv7.output_lab,
                         data_4D_unl_1=self.conv7.output_1,
                         data_4D_unl_2=self.conv7.output_2,
                         data_4D_unl_clean=self.conv7.output_clean,
                         noise_weight=self.noise_weights[7],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K8, M=self.M8, W=self.W8, H=self.H8,
                         w=self.w8, h=self.h8, Cin=self.Cin8, Ni=self.batch_size,
                         amps_val_init=amps_val_init[1], lambdas_val_init=lambdas_val_init[1],
                         gamma_val_init=gamma_val_init[1], beta_val_init=beta_val_init[1],
                         prun_mat_init=prun_mat_init[1], prun_synap_mat_init=prun_synap_mat_init[1],
                         mean_bn_init=mean_bn_init[1], var_bn_init=var_bn_init[1],
                         sigma_dn_init=sigma_dn_init[1], b_dn_init=b_dn_init[1], alpha_dn_init=alpha_dn_init[1],
                         pool_t_mode=None,
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[7], is_bn_BU=self.is_bn_BU,
                         nonlin=self.nonlin,
                         is_prun=self.is_prun,
                         is_prun_synap=self.is_prun_synap,
                         is_dn=self.is_dn,
                         update_mean_var_with_sup=self.update_mean_var_with_sup)

        self.conv8._E_step_Bottom_Up()

        self.conv9 = CRM(data_4D_lab=self.conv8.output_lab,
                         data_4D_unl_1=self.conv8.output_1,
                         data_4D_unl_2=self.conv8.output_2,
                         data_4D_unl_clean=self.conv8.output_clean,
                         noise_weight=self.noise_weights[8],
                         noise_std=self.noise_std,
                         is_train=self.is_train, momentum_bn=self.momentum_bn,
                         K=self.K9, M=self.M9, W=self.W9, H=self.H9,
                         w=self.w9, h=self.h9, Cin=self.Cin9, Ni=self.batch_size,
                         amps_val_init=amps_val_init[0], lambdas_val_init=lambdas_val_init[0],
                         gamma_val_init=gamma_val_init[0], beta_val_init=beta_val_init[0],
                         prun_mat_init=prun_mat_init[0], prun_synap_mat_init=prun_synap_mat_init[0],
                         mean_bn_init=mean_bn_init[0], var_bn_init=var_bn_init[0],
                         sigma_dn_init=sigma_dn_init[0], b_dn_init=b_dn_init[0], alpha_dn_init=alpha_dn_init[0],
                         pool_t_mode='mean_t', mean_pool_size=(6,6),
                         border_mode='valid',
                         max_condition_number=1.e3, xavier_init=xavier_init,
                         is_noisy=is_noisy[8], is_bn_BU=self.is_bn_BU,
                         nonlin=self.nonlin,
                         is_prun=self.is_prun,
                         is_prun_synap=self.is_prun_synap,
                         is_dn=self.is_dn,
                         update_mean_var_with_sup=self.update_mean_var_with_sup)

        self.conv9._E_step_Bottom_Up()

        self.RegressionSiamese = HiddenLayerInSoftmax(
            input_lab=T.abs_(self.conv9.output_1.flatten(2) - self.conv9.output_2.flatten(2)),
            input_unl=None,
            input_clean=None,
            n_in=self.Cin_Softmax, n_out=2,
            W_init=W_Siamese_init, b_init=b_Siamese_init)

        self.RegressionInSoftmax = HiddenLayerInSoftmaxSiamese(input_lab=self.conv9.output_lab.flatten(2),
                                                               input_unl_1=self.conv9.output_1.flatten(2),
                                                               input_unl_2=self.conv9.output_2.flatten(2),
                                                               input_clean=self.conv9.output_clean.flatten(2),
                                                               n_in=self.Cin_Softmax, n_out=self.K_Softmax,
                                                               W_init=W_init, b_init=b_init)

        softmax_input_lab = self.RegressionInSoftmax.output_lab
        softmax_input_1 = self.RegressionInSoftmax.output_1
        softmax_input_2 = self.RegressionInSoftmax.output_2
        softmax_input_clean = self.RegressionInSoftmax.output_clean

        # classify the values of the fully-connected sigmoidal layer
        self.softmax_layer_nonlin = SoftmaxNonlinearitySiamese(input_lab=softmax_input_lab,
                                                               input_unl_1=softmax_input_1,
                                                               input_unl_2=softmax_input_2,
                                                               input_clean=softmax_input_clean)

        self.softmax_siamese_nonlin = SoftmaxNonlinearity(input_lab=self.RegressionSiamese.output_lab,
                                                          input_unl=None,
                                                          input_clean=None)

        # Computing test and validation results
        self.cosine_sim_test = T.sum(self.conv9.output_1.flatten(2) * self.conv9.output_2.flatten(2), axis=1) \
                               / ((self.conv9.output_1.flatten(2).norm(L=2, axis=1)) * (self.conv9.output_2.flatten(2).norm(L=2, axis=1)))

        # Computing train results
        self.classification_error = self.softmax_siamese_nonlin.errors(self.y_siamese)
        self.classification_accu = 1. - self.classification_error

        self.layers = [self.conv9, self.conv8, self.conv7, self.conv6, self.conv5, self.conv4, self.conv3, self.conv2, self.conv1]
        self.layer_names = ['conv9', 'conv8', 'conv7', 'conv6', 'conv5', 'conv4', 'conv3', 'conv2', 'conv1']
        self.N_layer = len(self.layers)
        self.survive_thres = [0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0]

        # build Top-Down pass
        self.Build_TopDown_End_to_End()

        # build the cost function for the model
        self.Build_Cost()

        # build update rules for the model
        self.Build_Update_Rule(method=self.method)

        self.Collect_Monitored_Vars()

    def compute_one_hot(self, input):
        sampling_indx = T.argmax(input, axis=1)
        one_hot_vec = T.extra_ops.to_one_hot(sampling_indx, self.num_class)
        output = input - input * one_hot_vec
        return one_hot_vec, output