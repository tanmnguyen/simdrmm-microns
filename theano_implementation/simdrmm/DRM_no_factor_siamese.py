__author__ = 'tannguyen'

import matplotlib as mpl

mpl.use('Agg')
from pylab import *

from theano.tensor.signal import pool
from nn_functions_siamese import SoftmaxNonlinearity
from nn_functions_siamese import Adam, RMSprop
import theano.tensor as T

import copy

import theano


# from guppy import hpy; h=hpy()

class DRM_model(object):
    '''
    Class of DRM models
    '''

    def __init__(self, seed, train_mode='supervised', grad_min=-np.inf, grad_max=np.inf):
        self.layers = []
        self.softmax_layer_nonlin = SoftmaxNonlinearity(input_lab=[], input_clean=[])
        self.Cin_Softmax = 0
        self.H_Softmax = 0
        self.W_Softmax = 0
        self.train_mode = train_mode
        self.K_Softmax = 0
        self.W_softmax_init = None
        self.b_softmax_init = None
        self.N_layer = 0
        self.denoising = None
        self.reconst_weights = []
        self.grad_min = grad_min
        self.grad_max = grad_max
        self.N_params_per_layer = 0
        self.seed = seed
        self.prun_weights = [1.0, 1.0, 1.0, 1.0, 1.0]
        self.num_class = 10

        np.random.seed(self.seed)

        self.x = T.tensor4('x')
        self.y_lab = T.ivector('y_lab')
        self.lr = T.scalar('l_r', dtype=theano.config.floatX)
        self.prun_threshold = T.vector('prun_threshold', dtype=theano.config.floatX)
        self.is_train = T.iscalar('is_train')
        self.momentum_bn = T.scalar('momentum_bn', dtype=theano.config.floatX)

    def Build_TopDown_End_to_End(self):
        '''
        Build E_step TopDown including the Softmax Regression
        :return:
        '''
        self.top_output_1 = T.dot(T.extra_ops.to_one_hot(self.softmax_layer_nonlin.y_pred_1, self.num_class),
                                  self.RegressionInSoftmax.W.T)
        self.top_output_2 = T.dot(T.extra_ops.to_one_hot(self.softmax_layer_nonlin.y_pred_2, self.num_class),
                                  self.RegressionInSoftmax.W.T)
        self.top_output_lab = T.dot(
            T.extra_ops.to_one_hot(self.softmax_layer_nonlin.y_pred_lab, self.num_class),
            self.RegressionInSoftmax.W.T)

        self.top_output_1 = self.top_output_1.dimshuffle(0, 1, 'x', 'x')
        self.top_output_2 = self.top_output_2.dimshuffle(0, 1, 'x', 'x')
        self.top_output_lab = self.top_output_lab.dimshuffle(0, 1, 'x', 'x')

        self.layers[0]._E_step_Top_Down_Reconstruction(mu_cg_1=self.top_output_1, mu_cg_2=self.top_output_2, mu_cg_lab=self.top_output_lab)

        for i in xrange(1, self.N_layer):
            self.layers[i]._E_step_Top_Down_Reconstruction(mu_cg_1=self.layers[i - 1].data_reconstructed_1,
                                                           mu_cg_2=self.layers[i - 1].data_reconstructed_2,
                                                           mu_cg_lab=self.layers[i - 1].data_reconstructed_lab)

    def Build_Cost(self):
        '''
        Build the cost we minimize during training is the NLL of the  model
        :return:
        '''
        self.siamese_cost = self.softmax_siamese_nonlin.negative_log_likelihood(self.y_siamese)

        if self.is_sup:
            self.cost = self.siamese_cost
        else:
            # Build reconstruction and sign cost
            self.sign_cost_unl = 0.0
            self.sign_cost_lab = 0.0
            for i in xrange(self.N_layer):
                self.sign_cost_unl += (T.mean(T.nnet.relu(-self.layers[i].latents_unpooled_no_mask_1) ** 2) \
                                  + T.mean(T.nnet.relu(-self.layers[i].latents_unpooled_no_mask_2) ** 2))/2.
                self.sign_cost_lab += T.mean(T.nnet.relu(-self.layers[i].latents_unpooled_no_mask_lab) ** 2)

            # unsupervised learning cost
            self.unsupervised_cost_unl = (T.mean((self.layers[-1].data_4D_unl_1 - self.layers[-1].data_reconstructed_1) ** 2) \
                                     + T.mean((self.layers[-1].data_4D_unl_2 - self.layers[-1].data_reconstructed_2) ** 2))/2.
            self.unsupervised_cost_lab = T.mean(
                (self.layers[-1].data_4D_lab - self.layers[-1].data_reconstructed_lab) ** 2)

            # KL divergence cost
            self.KLD_cost_unl = (-T.mean(T.sum(T.log(np.float32(self.num_class) * self.softmax_layer_nonlin.gammas_1 + 1e-8)
                                         * self.softmax_layer_nonlin.gammas_1, axis=1)) \
                            -T.mean(T.sum(T.log(np.float32(self.num_class) * self.softmax_layer_nonlin.gammas_2 + 1e-8)
                                         * self.softmax_layer_nonlin.gammas_2, axis=1)))/2.

            self.KLD_cost_lab = -T.mean(
                T.sum(T.log(np.float32(self.num_class) * self.softmax_layer_nonlin.gammas_lab + 1e-8)
                      * self.softmax_layer_nonlin.gammas_lab, axis=1))

            if self.cost_mode == 'both':
                self.sign_cost = 0.5 * self.sign_cost_unl + 0.5 * self.sign_cost_lab
                self.KLD_cost = 0.5 * self.KLD_cost_unl + 0.5 * self.KLD_cost_lab
                self.unsupervised_cost = 0.5 * self.unsupervised_cost_unl + 0.5 * self.unsupervised_cost_lab
                self.L1reg = 0.5 * self.L1reg_unl + 0.5 * self.L1reg_lab
            else:
                self.sign_cost = self.sign_cost_unl
                self.KLD_cost = self.KLD_cost_unl
                self.unsupervised_cost = self.unsupervised_cost_unl
                self.L1reg = self.L1reg_unl

            # Total cost
            self.cost = self.siamese_cost \
                        + self.reconst_weights * self.unsupervised_cost \
                        + self.KL_coef * self.KLD_cost \
                        + self.sign_cost_weight * self.sign_cost

    def Build_Update_Rule(self, method='SGD'):
        # Specify update rules and outputs
        self.params = []
        for i in xrange(self.N_layer):
            self.params = self.params + self.layers[i].params

        if self.is_sup:
            self.params = self.params + self.RegressionSiamese.params
        else:
            self.params = self.params + self.RegressionSiamese.params
            self.params = self.params + self.RegressionInSoftmax.params

        self.grads = T.grad(self.cost, self.params)  # create a list of gradients for all model parameters

        if method == 'SGD': # use SGD to train the DRMM
            self.updates = []
            for param_i, grad_i in zip(self.params, self.grads):
                self.updates.append((param_i, param_i - self.lr * T.clip(grad_i, self.grad_min, self.grad_max)))

        elif method == 'adam': # use adam to train the DRMM
            self.updates = Adam(self.cost, self.params, lr=self.lr, b1=0.1, b2=0.001, e=1e-8)

        elif method == 'rmsprop': # use rmsprop to train the DRMM
            self.updates = RMSprop(cost=self.cost, params=self.params, lr=self.lr, rho=0.9, epsilon=1e-6)

        else:
            raise

        self.updates_after_train = []
        # for pruning
        self.updates_pi_old = []
        self.updates_pi = []
        self.correct_pi = []
        self.updates_pruning = []
        self.update_for_dn = []

        for l in range(len(self.layers)):
            # Concatenate updates
            self.updates.append((self.layers[l].amps, self.layers[l].amps_new))
            self.updates.append((self.layers[l].pi_t, self.layers[l].pi_t_new))
            self.updates.append((self.layers[l].pi_a, self.layers[l].pi_a_new))
            self.updates.append((self.layers[l].pi_ta, self.layers[l].pi_ta_new))
            if self.layers[l].is_bn_BU:
                self.updates.append((self.layers[l].bn_BU.mean, self.layers[l].bn_BU.mean_new))
                self.updates.append((self.layers[l].bn_BU.var, self.layers[l].bn_BU.var_new))

            if self.layers[l].is_dn:
                self.update_for_dn.append((self.layers[l].sigma_dn, T.abs_(self.layers[l].sigma_dn)))
                self.update_for_dn.append((self.layers[l].alpha_dn, T.nnet.relu(self.layers[l].alpha_dn)))

            if self.is_prun:
                self.updates_pi_old.append((self.layers[l].pi_a_old, self.layers[l].pi_a))
                self.updates_pi.append((self.layers[l].pi_a, self.layers[l].pi_a
                                        + self.layers[l].pi_a_minibatch))
                self.correct_pi.append(
                    (self.layers[l].pi_a, (self.layers[l].pi_a - self.layers[l].pi_a_old) / 100.0))

                self.updates_pruning.append((self.layers[l].prun_mat,
                                             self.layers[l].prun_mat * T.cast(T.ge(self.layers[l].pi_a,
                                                                                   self.prun_weights[l] *
                                                                                   self.prun_threshold[l]),
                                                                              theano.config.floatX)))

            if self.is_prun_synap:
                self.updates_pi_old.append((self.layers[l].pi_synap_old, self.layers[l].pi_synap))
                self.updates_pi.append((self.layers[l].pi_synap, self.layers[l].pi_synap
                                        + self.layers[l].pi_synap_minibatch))
                self.correct_pi.append(
                    (self.layers[l].pi_synap, (self.layers[l].pi_synap - self.layers[l].pi_synap_old) / 100.0))

                self.updates_pruning.append((self.layers[l].prun_synap_mat,
                                             self.layers[l].prun_synap_mat * T.cast(T.ge(self.layers[l].pi_synap,
                                                                                         self.prun_weights[l] *
                                                                                         self.prun_threshold[l]),
                                                                                    theano.config.floatX)))

            if self.is_prun_songhan:
                self.updates_pruning.append((self.layers[l].prun_synap_mat,
                                             self.layers[l].prun_synap_mat
                                             * T.cast(T.ge(T.abs_(T.reshape(self.layers[l].betas[:, 0, :],
                                                                            newshape=(
                                                                            self.layers[l].K, self.layers[l].Cin,
                                                                            self.layers[l].h, self.layers[l].w))),
                                                           self.prun_weights[l] * self.prun_threshold[l]
                                                           * T.std(T.reshape(self.layers[l].betas[:, 0, :],
                                                                             newshape=(
                                                                             self.layers[l].K, self.layers[l].Cin,
                                                                             self.layers[l].h, self.layers[l].w)))),
                                                      theano.config.floatX)))

            # Concatenate updates after train
            self.updates_after_train.append((self.layers[l].pi_t_final, self.layers[l].pi_t_final + self.layers[l].pi_t_minibatch))
            self.updates_after_train.append((self.layers[l].pi_a_final, self.layers[l].pi_a_final + self.layers[l].pi_a_minibatch))

    def Collect_Monitored_Vars(self):
        self.all_layer_names = copy.copy(self.layer_names)
        self.all_layer_names.append('softmax')

        # list of params we want to monitor
        if self.is_bn_BU:
            if self.is_prun:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'bn-gamma', 'bn-beta', 'prun-mat']
            elif self.is_prun_synap:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'bn-gamma', 'bn-beta', 'prun-synap-mat',
                                            'pi-synap']
            elif self.is_prun_songhan:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'bn-gamma', 'bn-beta', 'prun-synap-mat']
            else:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'bn-gamma', 'bn-beta']
        elif self.is_dn:
            if self.is_prun:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'sigma-dn', 'b-dn', 'alpha-dn', 'prun-mat']
            elif self.is_prun_synap:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'sigma-dn', 'b-dn', 'alpha-dn', 'prun-synap-mat',
                                            'pi-synap']
            elif self.is_prun_songhan:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'sigma-dn', 'b-dn', 'alpha-dn', 'prun-synap-mat']
            else:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'sigma-dn', 'b-dn', 'alpha-dn']
        else:
            self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta']
            if self.is_prun:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'prun-mat']
            elif self.is_prun_synap:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'prun-synap-mat', 'pi-synap']
            elif self.is_prun_songhan:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta', 'prun-synap-mat']
            else:
                self.monitor_param_names = ['lambdas', 'pi-a', 'pi-t', 'pi-ta']

        self.monitor_params = []
        for l in range(self.N_layer):
            self.monitor_params.append(self.layers[l].lambdas)
            self.monitor_params.append(self.layers[l].pi_a)
            self.monitor_params.append(self.layers[l].pi_t)
            self.monitor_params.append(self.layers[l].pi_ta)
            if self.is_bn_BU:
                self.monitor_params.append(self.layers[l].bn_BU.gamma)
                self.monitor_params.append(self.layers[l].bn_BU.beta)
            elif self.is_dn:
                self.monitor_params.append(self.layers[l].sigma_dn)
                self.monitor_params.append(self.layers[l].b_dn)
                self.monitor_params.append(self.layers[l].alpha_dn)
            if self.is_prun:
                self.monitor_params.append(self.layers[l].prun_mat)
            elif self.is_prun_synap:
                self.monitor_params.append(self.layers[l].prun_synap_mat)
                self.monitor_params.append(self.layers[l].pi_synap)
            elif self.is_prun_songhan:
                self.monitor_params.append(self.layers[l].prun_synap_mat)

        # list of softmax params we want to monitor
        self.monitor_param_softmax_names = ['W', 'b', 'Wsiamese', 'bsiamese']
        self.monitor_params.append(self.RegressionInSoftmax.W)
        self.monitor_params.append(self.RegressionInSoftmax.b)
        self.monitor_params.append(self.RegressionSiamese.W)
        self.monitor_params.append(self.RegressionSiamese.b)

        # list of costs that we want to monitor
        if self.is_sup:
            self.monitor_cost_names = ['total-cost', 'siamese-cost', 'classification-accuracy',
                                       'classification-error']
            self.monitor_costs = T.as_tensor_variable([self.cost, self.siamese_cost, self.classification_accu, self.classification_error])
        else:
            self.monitor_cost_names = ['total-cost', 'siamese-cost', 'unsupervised-cost', 'KLD-cost',
                                       'sign-cost', 'classification-accuracy',
                                       'classification-error']
            self.monitor_costs = T.as_tensor_variable(
                [self.cost, self.siamese_cost, self.unsupervised_cost,
                 self.KLD_cost, self.sign_cost, self.classification_accu, self.classification_error])

        self.monitor_test_valid_cost_names = ['5-way-error',]

        if self.is_sup:
            # list of 4D vars we want to monitor + self.monitor_costs at the end
            self.monitor_var_names = ['latents_before_BN','latents','max_over_a_mask','max_over_t_mask','latents-masked',
                                      'latents-after-pooled', 'scale-s', 'latents_demeaned', 'latents_demeaned_squared']
            self.monitor_vars = []
            for l in range(self.N_layer):
                self.monitor_vars.append(self.layers[l].latents_before_BN_1)
                self.monitor_vars.append(self.layers[l].latents_1)
                self.monitor_vars.append(self.layers[l].max_over_a_mask_1)
                self.monitor_vars.append(self.layers[l].max_over_t_mask_1)
                self.monitor_vars.append(self.layers[l].latents_masked_1)
                self.monitor_vars.append(self.layers[l].output_1)
                self.monitor_vars.append(self.layers[l].scale_s_1)
                self.monitor_vars.append(self.layers[l].latents_demeaned_1)
                self.monitor_vars.append(self.layers[l].latents_demeaned_squared_1)
        else:
            # list of 4D vars we want to monitor + self.monitor_costs at the end
            self.monitor_var_names = ['latents_before_BN', 'latents', 'max_over_a_mask', 'max_over_t_mask',
                                      'latents-masked',
                                      'latents-after-pooled', 'latents_unpooled', 'data-reconstructed', 'scale-s',
                                      'latents_demeaned', 'latents_demeaned_squared']
            self.monitor_vars = []
            for l in range(self.N_layer):
                self.monitor_vars.append(self.layers[l].latents_before_BN_1)
                self.monitor_vars.append(self.layers[l].latents_1)
                self.monitor_vars.append(self.layers[l].max_over_a_mask_1)
                self.monitor_vars.append(self.layers[l].max_over_t_mask_1)
                self.monitor_vars.append(self.layers[l].latents_masked_1)
                self.monitor_vars.append(self.layers[l].output_1)
                self.monitor_vars.append(self.layers[l].latents_unpooled_1)
                self.monitor_vars.append(self.layers[l].data_reconstructed_1)
                self.monitor_vars.append(self.layers[l].scale_s_1)
                self.monitor_vars.append(self.layers[l].latents_demeaned_1)
                self.monitor_vars.append(self.layers[l].latents_demeaned_squared_1)

        self.pi_minibatch_vars = []
        if self.is_prun_synap:
            for l in range(self.N_layer):
                self.pi_minibatch_vars.append(self.layers[l].pi_synap_minibatch)
        elif self.is_prun:
            for l in range(self.N_layer):
                self.pi_minibatch_vars.append(self.layers[l].pi_a_minibatch)

        # list of vars from the SoftMax layer
        self.monitor_vars_softmax = [self.RegressionInSoftmax.output_1,
                                     self.softmax_layer_nonlin.gammas_1,
                                     self.softmax_layer_nonlin.y_pred_1]

        self.monitor_var_softmax_names = ['score', 'posterior', 'label-predicted']


        self.N_softmax_vars = len(self.monitor_vars_softmax)

        # append monitor_vars_softmax into monitor_vars
        for i in range(self.N_softmax_vars):
            self.monitor_vars.append(self.monitor_vars_softmax[i])

        # Note that self.monitor_vars also includes self.monitor_costs
        self.monitor_vars.append(self.monitor_costs)

        # list of stats that we want to compute with monitor_vars but not monitor_costs
        # note that histograms will be computed inside train_no_factor_latest separately
        self.monitor_stat_names = ['min', 'max', 'var', 'mean', 'sparsity']
        self.is_plot_histogram = False


