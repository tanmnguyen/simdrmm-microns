import numpy
import numpy as np

import theano
import theano.tensor as T


class SoftmaxNonlinearity(object):
    def __init__(self, input_lab, input_unl, input_clean=None):
        self.gammas = None
        self.gammas_clean = None
        self.gammas_lab = None
        self.y_pred = None
        self.y_pred_clean = None
        self.y_pred_lab = None

        if input_lab != None:
            self.gammas_lab = T.nnet.softmax(input_lab)
            self.y_pred_lab = T.argmax(self.gammas_lab, axis=1)

        if input_unl != None:
            self.gammas = T.nnet.softmax(input_unl)
            self.y_pred = T.argmax(self.gammas, axis=1)

        if input_clean != None:
            # compute vector of class-membership probabilities in symbolic form
            self.gammas_clean =  T.nnet.softmax(input_clean)
            # compute prediction as class whose probability is maximal in
            # symbolic form
            self.y_pred_clean = T.argmax(self.gammas_clean, axis=1)

    def negative_log_likelihood(self, y):
        """Return the mean of the negative log-likelihood of the prediction
        of this model under a given target distribution.

        .. math::

            \frac{1}{|\mathcal{D}|} \mathcal{L} (\theta=\{W,b\}, \mathcal{D}) =
            \frac{1}{|\mathcal{D}|} \sum_{i=0}^{|\mathcal{D}|} \log(P(Y=y^{(i)}|x^{(i)}, W,b)) \\
                \ell (\theta=\{W,b\}, \mathcal{D})

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label

        Note: we use the mean instead of the sum so that
              the learning rate is less dependent on the batch size
        """
        # y.shape[0] is (symbolically) the number of rows in y, i.e.,
        # number of examples (call it n) in the minibatch
        # T.arange(y.shape[0]) is a symbolic vector which will contain
        # [0,1,2,... n-1] T.log(self.p_y_given_x) is a matrix of
        # Log-Probabilities (call it LP) with one row per example and
        # one column per class LP[T.arange(y.shape[0]),y] is a vector
        # v containing [LP[0,y[0]], LP[1,y[1]], LP[2,y[2]], ...,
        # LP[n-1,y[n-1]]] and T.mean(LP[T.arange(y.shape[0]),y]) is
        # the mean (across minibatch examples) of the elements in v,
        # i.e., the mean log-likelihood across the minibatch.
        return -T.mean(T.log(self.gammas_lab)[T.arange(y.shape[0]), y])

    def errors(self, y):
        """Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        """

        # check if y has same dimension of y_pred
        if y.ndim != self.y_pred_lab.ndim:
            raise TypeError(
                'y should have the same shape as self.y_pred',
                ('y', y.type, 'y_pred', self.y_pred.type)
            )
        # check if y is of the correct datatype
        if y.dtype.startswith('int'):
            # the T.neq operator returns a vector of 0s and 1s, where 1
            # represents a mistake in prediction
            return T.mean(T.cast(T.neq(self.y_pred_lab, y), dtype=theano.config.floatX))
        else:
            raise NotImplementedError()

class SoftmaxNonlinearitySiamese(object):
    def __init__(self, input_lab, input_unl_1, input_unl_2, input_clean=None):
        self.gammas_1 = None
        self.gammas_2 = None
        self.gammas_clean = None
        self.gammas_lab = None
        self.y_pred_1 = None
        self.y_pred_2 = None
        self.y_pred_clean = None
        self.y_pred_lab = None

        if input_lab != None:
            self.gammas_lab = T.nnet.softmax(input_lab)
            self.y_pred_lab = T.argmax(self.gammas_lab, axis=1)

        if input_unl_1 != None:
            self.gammas_1 = T.nnet.softmax(input_unl_1)
            self.y_pred_1 = T.argmax(self.gammas_1, axis=1)

        if input_unl_2 != None:
            self.gammas_2 = T.nnet.softmax(input_unl_2)
            self.y_pred_2 = T.argmax(self.gammas_2, axis=1)

        if input_clean != None:
            # compute vector of class-membership probabilities in symbolic form
            self.gammas_clean =  T.nnet.softmax(input_clean)
            # compute prediction as class whose probability is maximal in
            # symbolic form
            self.y_pred_clean = T.argmax(self.gammas_clean, axis=1)

    def negative_log_likelihood(self, y):
        """Return the mean of the negative log-likelihood of the prediction
        of this model under a given target distribution.

        .. math::

            \frac{1}{|\mathcal{D}|} \mathcal{L} (\theta=\{W,b\}, \mathcal{D}) =
            \frac{1}{|\mathcal{D}|} \sum_{i=0}^{|\mathcal{D}|} \log(P(Y=y^{(i)}|x^{(i)}, W,b)) \\
                \ell (\theta=\{W,b\}, \mathcal{D})

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label

        Note: we use the mean instead of the sum so that
              the learning rate is less dependent on the batch size
        """
        # y.shape[0] is (symbolically) the number of rows in y, i.e.,
        # number of examples (call it n) in the minibatch
        # T.arange(y.shape[0]) is a symbolic vector which will contain
        # [0,1,2,... n-1] T.log(self.p_y_given_x) is a matrix of
        # Log-Probabilities (call it LP) with one row per example and
        # one column per class LP[T.arange(y.shape[0]),y] is a vector
        # v containing [LP[0,y[0]], LP[1,y[1]], LP[2,y[2]], ...,
        # LP[n-1,y[n-1]]] and T.mean(LP[T.arange(y.shape[0]),y]) is
        # the mean (across minibatch examples) of the elements in v,
        # i.e., the mean log-likelihood across the minibatch.
        return -T.mean(T.log(self.gammas_lab)[T.arange(y.shape[0]), y])

    def errors(self, y):
        """Return a float representing the number of errors in the minibatch
        over the total number of examples of the minibatch ; zero one
        loss over the size of the minibatch

        :type y: theano.tensor.TensorType
        :param y: corresponds to a vector that gives for each example the
                  correct label
        """

        # check if y has same dimension of y_pred
        if y.ndim != self.y_pred_1.ndim:
            raise TypeError(
                'y should have the same shape as self.y_pred',
                ('y', y.type, 'y_pred', self.y_pred_1.type)
            )
        # check if y is of the correct datatype
        if y.dtype.startswith('int'):
            # the T.neq operator returns a vector of 0s and 1s, where 1
            # represents a mistake in prediction
            return T.mean(T.cast(T.neq(self.y_pred_lab, y), dtype=theano.config.floatX))
        else:
            raise NotImplementedError()

class HiddenLayerInSoftmax(object):
    def __init__(self, input_lab, input_unl, n_in, n_out, W_init=None, b_init=None, input_clean=None):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The nonlinearity used here is tanh

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type input_lab, input_unl, input_clean: theano.tensor.dmatrix
        :param input_lab, input_unl, input_clean: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        """
        if W_init == None:
            W_bound = np.sqrt(1. / n_in)
            W_val = np.asarray(np.random.uniform(low=-W_bound, high=W_bound, size=(n_in, n_out)),dtype=theano.config.floatX)
            self.W = theano.shared(value=W_val, name='W', borrow=True)

            # self.W = theano.shared(value=numpy.zeros((n_in, n_out),
            #                                          dtype=theano.config.floatX) + 1e-8,
            #                        name='W', borrow=True)
        else:
            self.W = theano.shared(value=W_init, name='W', borrow=True)
        # initialize the baises b as a vector of n_out 0s
        if b_init == None:
            self.b = theano.shared(value=numpy.zeros((n_out,),
                                                     dtype=theano.config.floatX) + 1e-8,
                                   name='b', borrow=True)
        else:
            self.b = theano.shared(value=b_init, name='b', borrow=True)

        self.output_lab = None
        self.output = None
        self.output_clean = None

        if input_lab != None:
            self.output_lab = T.dot(input_lab, self.W) + self.b

        if input_unl != None:
            self.output = T.dot(input_unl, self.W) + self.b

        if input_clean != None:
            self.output_clean = T.dot(input_clean, self.W) + self.b

        # parameters of the model
        self.params = [self.W, self.b]

class HiddenLayerInSoftmaxSiamese(object):
    def __init__(self, input_lab, input_unl_1, input_unl_2, n_in, n_out, W_init=None, b_init=None, input_clean=None):
        """
        Typical hidden layer of a MLP: units are fully-connected and have
        sigmoidal activation function. Weight matrix W is of shape (n_in,n_out)
        and the bias vector b is of shape (n_out,).

        NOTE : The nonlinearity used here is tanh

        Hidden unit activation is given by: tanh(dot(input,W) + b)

        :type input_lab, input_unl, input_clean: theano.tensor.dmatrix
        :param input_lab, input_unl, input_clean: a symbolic tensor of shape (n_examples, n_in)

        :type n_in: int
        :param n_in: dimensionality of input

        :type n_out: int
        :param n_out: number of hidden units

        """
        if W_init == None:
            W_bound = np.sqrt(1. / n_in)
            W_val = np.asarray(np.random.uniform(low=-W_bound, high=W_bound, size=(n_in, n_out)),dtype=theano.config.floatX)
            self.W = theano.shared(value=W_val, name='W', borrow=True)

            # self.W = theano.shared(value=numpy.zeros((n_in, n_out),
            #                                          dtype=theano.config.floatX) + 1e-8,
            #                        name='W', borrow=True)
        else:
            self.W = theano.shared(value=W_init, name='W', borrow=True)
        # initialize the baises b as a vector of n_out 0s
        if b_init == None:
            self.b = theano.shared(value=numpy.zeros((n_out,),
                                                     dtype=theano.config.floatX) + 1e-8,
                                   name='b', borrow=True)
        else:
            self.b = theano.shared(value=b_init, name='b', borrow=True)

        self.output_lab = None
        self.output_1 = None
        self.output_2 = None
        self.output_clean = None

        if input_lab != None:
            self.output_lab = T.dot(input_lab, self.W) + self.b

        if input_unl_1 != None:
            self.output_1 = T.dot(input_unl_1, self.W) + self.b

        if input_unl_2 != None:
            self.output_2 = T.dot(input_unl_2, self.W) + self.b

        if input_clean != None:
            self.output_clean = T.dot(input_clean, self.W) + self.b

        # parameters of the model
        self.params = [self.W, self.b]

class BatchNormalization(object):
    def __init__(self, insize, momentum, is_train, mode=0, epsilon=1e-10, gamma_val_init=None, beta_val_init=None,
                 mean_init=None, var_init=None):
        '''
        # params :
        input_shape :
            when mode is 0, we assume 2D input. (mini_batch_size, # features)
            when mode is 1, we assume 4D input. (mini_batch_size, # of channel, # row, # column)
        mode :
            0 : feature-wise mode (normal BN)
            1 : window-wise mode (CNN mode BN)
        momentum : momentum for exponential average.
        '''
        self.insize = insize
        self.mode = mode
        self.momentum = momentum
        self.is_train = is_train
        self.run_mode = 0  # run_mode :
        self.epsilon = epsilon

        # random setting of gamma and beta, setting initial mean and std
        # rng = np.random.RandomState(int(time.time()))
        # self.gamma = theano.shared(np.asarray(
        #     rng.uniform(low=-1.0 / math.sqrt(self.insize), high=1.0 / math.sqrt(self.insize), size=(insize)),
        #     dtype=theano.config.floatX), name='gamma', borrow=True)
        if gamma_val_init == None:
            self.gamma = theano.shared(np.ones((insize), dtype=theano.config.floatX), name='gamma_bn', borrow=True)
        else:
            self.gamma = theano.shared(gamma_val_init, name='gamma_bn', borrow=True)

        if beta_val_init == None:
            self.beta = theano.shared(np.zeros((insize), dtype=theano.config.floatX), name='beta_bn', borrow=True)
        else:
            self.beta = theano.shared(beta_val_init, name='beta_bn', borrow=True)

        if mean_init == None:
            self.mean = theano.shared(np.zeros((insize), dtype=theano.config.floatX), name='mean_bn', borrow=True)
        else:
            self.mean = theano.shared(mean_init, name='mean_bn', borrow=True)

        if var_init == None:
            self.var = theano.shared(np.ones((insize), dtype=theano.config.floatX), name='var_bn', borrow=True)
        else:
            self.var = theano.shared(var_init, name='var_bn', borrow=True)

        # parameter save for update
        self.params = [self.gamma, self.beta]

    def set_runmode(self, run_mode):
        self.run_mode = run_mode

    def get_result(self, input, input_shape):
        # returns BN result for given input.

        if self.mode == 0:
            # print('Use Feature-wise BN')
            if self.run_mode == 0:
                now_mean = T.mean(input, axis=0)
                now_var = T.var(input, axis=0)
                now_normalize = (input - now_mean) / T.sqrt(now_var + self.epsilon)  # should be broadcastable..
                output = self.gamma * now_normalize + self.beta
                # mean, var update
                self.mean = self.momentum * self.mean + (1.0 - self.momentum) * now_mean
                self.var = self.momentum * self.var \
                           + (1.0 - self.momentum) * (input_shape[0] / (input_shape[0] - 1) * now_var)
            elif self.run_mode == 1:
                now_mean = T.mean(input, axis=0)
                now_var = T.var(input, axis=0)
                output = self.gamma * (input - now_mean) / T.sqrt(now_var + self.epsilon) + self.beta
            else:
                now_mean = self.mean
                now_var = self.var
                output = self.gamma * (input - now_mean) / T.sqrt(now_var + self.epsilon) + self.beta

        else:
            # in CNN mode, gamma and beta exists for every single channel separately.
            # for each channel, calculate mean and std for (mini_batch_size * row * column) elements.
            # then, each channel has own scalar gamma/beta parameters.
            # print('Use Layer-wise BN')
            if self.run_mode == 0:
                now_mean = T.mean(input, axis=(0, 2, 3))
                now_var = T.var(input, axis=(0, 2, 3))
                # now_mean_new_shape = self.change_shape(now_mean)
                # now_var = T.sqr(T.mean(T.abs_(input - now_mean_new_shape), axis=(0,2,3)))

                # mean, var update
                self.mean_new = self.momentum * self.mean + (1.0 - self.momentum) * now_mean
                self.var_new = self.momentum * self.var \
                           + (1.0 - self.momentum) * (input_shape[0] / (input_shape[0] - 1 + 1e-5) * now_var)

                now_mean_4D = T.switch(T.neq(self.is_train, 0),self.change_shape(now_mean, input_shape),
                                       self.change_shape(self.mean_new, input_shape))
                now_var_4D = T.switch(T.neq(self.is_train, 0),self.change_shape(now_var, input_shape),
                                      self.change_shape(self.var_new, input_shape))

                now_gamma_4D = self.change_shape(self.gamma, input_shape)
                now_beta_4D = self.change_shape(self.beta, input_shape)

                output = now_gamma_4D * (input - now_mean_4D) / T.sqrt(now_var_4D + self.epsilon) + now_beta_4D

            else:
                now_mean = T.mean(input, axis=(0, 2, 3))
                now_var = T.var(input, axis=(0, 2, 3))
                # now_mean_new_shape = self.change_shape(now_mean)
                # now_var = T.sqr(T.mean(T.abs_(input - now_mean_new_shape), axis=(0, 2, 3)))

                now_mean_4D = T.switch(T.neq(self.is_train, 0), self.change_shape(now_mean, input_shape),
                                       self.change_shape(self.mean, input_shape))
                now_var_4D = T.switch(T.neq(self.is_train, 0), self.change_shape(now_var, input_shape),
                                      self.change_shape(self.var, input_shape))

                now_gamma_4D = self.change_shape(self.gamma, input_shape)
                now_beta_4D = self.change_shape(self.beta, input_shape)

                output = now_gamma_4D * (input - now_mean_4D) / T.sqrt(now_var_4D + self.epsilon) + now_beta_4D

        return output

    # changing shape for CNN mode
    def change_shape(self, vec, input_shape):
        return T.repeat(vec, input_shape[2] * input_shape[3]).reshape(
            (input_shape[1], input_shape[2], input_shape[3]))


def Adam(cost, params, lr=0.0002, b1=0.1, b2=0.001, e=1e-8):
    updates = []
    grads = T.grad(cost, params)
    i = theano.shared(np.float32(0.))
    i_t = i + 1.
    fix1 = 1. - (1. - b1) ** i_t
    fix2 = 1. - (1. - b2) ** i_t
    lr_t = lr * (T.sqrt(fix2) / fix1)
    for p, g in zip(params, grads):
        m = theano.shared(p.get_value() * 0.)
        v = theano.shared(p.get_value() * 0.)
        m_t = (b1 * g) + ((1. - b1) * m)
        v_t = (b2 * T.sqr(g)) + ((1. - b2) * v)
        g_t = m_t / (T.sqrt(v_t) + e)
        p_t = p - (lr_t * g_t)
        updates.append((m, m_t))
        updates.append((v, v_t))
        updates.append((p, p_t))
    updates.append((i, i_t))
    return updates

def RMSprop(cost, params, lr=0.001, rho=0.9, epsilon=1e-6):
    grads = T.grad(cost=cost, wrt=params)
    updates = []
    for p, g in zip(params, grads):
        acc = theano.shared(p.get_value() * 0.)
        acc_new = rho * acc + (1 - rho) * g ** 2
        gradient_scaling = T.sqrt(acc_new + epsilon)
        g = g / gradient_scaling
        updates.append((acc, acc_new))
        updates.append((p, p - lr * g))
    return updates
