from __future__ import print_function
__author__ = 'tannguyen'

import matplotlib as mpl
mpl.use('Agg')
from pylab import *

import os

import numpy as np
import scipy as sp

import theano
import theano.tensor as T

import copy
import cPickle
from theano.misc.pkl_utils import dump

from siamese_data import SiameseData

from numpy.linalg import norm
import time

# from theano.compile.nanguardmode import NanGuardMode


# from guppy import hpy; h=hpy()

# def detect_nan(i, node, fn):
#     for output in fn.outputs:
#         if (not isinstance(output[0], np.random.RandomState) and
#             np.isnan(output[0]).any()):
#             print('*** NaN detected ***')
#             theano.printing.debugprint(node)
#             print('Inputs : %s' % [input[0] for input in fn.inputs])
#             print('Outputs: %s' % [output[0] for output in fn.outputs])
#             break

class TrainNoFactorDRM(object):
    """
    Training algorithm class

    """
    def __init__(self, batch_size, model, output_dir, data_dir, N_train, N_test, N_valid,
                 seed, prun_weights, prun_rates, prun_freq,
                 param_dir='params', is_plot_full=True, is_prun=False, is_prun_to_end=True,
                 is_prun_songhan=False):
        '''

        :param data_mode:
        :param em_mode:
        :param update_mode:
        :param stop_mode:
        :param train_method:
        :param Ni:
        :param Cin:
        :param H:
        :param W:
        :param batch_size:
        :param seed:
        :param output_dir:
        :param data_dir:
        :return:
        '''

        self.siamese_dat = SiameseData(data_dir)
        self.batch_size = batch_size
        self.N_train = N_train
        self.N_test = N_test
        self.N_valid = N_valid

        self.seed = seed
        self.rng = np.random.RandomState(self.seed)
        self.prun_weights = prun_weights
        self.prun_rates = prun_rates
        self.is_prun = is_prun
        self.is_prun_songhan = is_prun_songhan
        self.prun_freq = prun_freq
        print('is_prun')
        print(self.is_prun)
        time.sleep(1)

        self.is_plot_full = is_plot_full
        self.is_prun_to_end = is_prun_to_end

        self.n_train_batches = N_train / self.batch_size
        self.n_valid_batches = N_valid / self.batch_size
        self.n_test_batches = N_test / self.batch_size

        self.model = model
        self.output_dir = output_dir
        self.param_dir = os.path.join(self.output_dir, param_dir)
        self.viz_dir = os.path.join(self.output_dir, 'img')
        self.cost_dir = os.path.join(self.viz_dir, 'cost')
        self.hist_dir = os.path.join(self.viz_dir, 'histogram')
        self.stat_dir = os.path.join(self.viz_dir, 'statistics')
        self.convergence_dir = os.path.join(self.viz_dir, 'convergence')
        if not os.path.exists(self.output_dir):
            os.makedirs(self.output_dir)
        if not os.path.exists(self.param_dir):
            os.makedirs(self.param_dir)
        if not os.path.exists(self.viz_dir):
            os.makedirs(self.viz_dir)
        if not os.path.exists(self.cost_dir):
            os.makedirs(self.cost_dir)
        if not os.path.exists(self.hist_dir):
            os.makedirs(self.hist_dir)
        if not os.path.exists(self.stat_dir):
            os.makedirs(self.stat_dir)
        if not os.path.exists(self.convergence_dir):
            os.makedirs(self.convergence_dir)

        with open(self.output_dir + '/log.txt', 'w+') as f:
            f.write('Write print out from the training\n')

    ####################################################################################################################
    # Train Semisupervised #
    ####################################################################################################################
    def train_semisupervised(self, max_epochs, lr_init=0.1, lr_final=0.0001, momentum_bn=0.99):
        #
        # Train semisupervised
        #
        # start_time_test = time.time()
        # dat, debug_info = self.siamese_dat.get_test_triplet(batch_size=self.N_test, label_type="specific",
        #                                                     K=None, num_choices=2)
        # stop_time_test = time.time()
        # print('load test data take %f seconds'%(stop_time_test - start_time_test))
        #
        # start_time_train = time.time()
        # train_dat, debug_info = self.siamese_dat.get_training_pair(batch_size=self.N_train, ratio=0.5,
        #                                                            label_type="specific", K=None)
        # stop_time_train = time.time()
        # print('load train data take %f seconds' % (stop_time_train - start_time_train))
        #
        # import pdb; pdb.set_trace()

        print('Start training semisupervised')

        self.max_epochs = max_epochs
        self.lr_init = lr_init
        self.lr_final = lr_final
        self.momentum_bn = momentum_bn

        # Save meta-parameters
        self.save_meta_params(filename='model-and-training-meta-params.npz')

        # Building function used during training. Note that self.monitor_vars also includes self.monitor_costs
        # update after train mini-batches

        if self.model.is_bn_BU:
            self.train_batch = theano.function(inputs=[self.model.x_unl_1, self.model.x_unl_2,
                                                       self.model.y_siamese, self.model.lr,
                                                       self.model.is_train, self.model.momentum_bn],
                                               outputs=self.model.monitor_vars,
                                               updates=self.model.updates,
                                               on_unused_input='warn')

            # compute costs and other stuffs for test and validation
            self.test_val_batch = theano.function(
                inputs=[self.model.x_unl_1, self.model.x_unl_2, self.model.is_train, self.model.momentum_bn],
                outputs=self.model.cosine_sim_test,
                updates=[],
                on_unused_input='warn')
        else:
            self.train_batch = theano.function(inputs=[self.model.x_unl_1, self.model.x_unl_2,
                                                       self.model.y_siamese, self.model.lr],
                                               outputs=self.model.monitor_vars,
                                               updates=self.model.updates,
                                               on_unused_input='warn')

            # compute costs and other stuffs for test and validation
            self.test_val_batch = theano.function(
                inputs=[self.model.x_unl_1, self.model.x_unl_2],
                outputs=self.model.cosine_sim_test,
                updates=[],
                on_unused_input='warn')

        if self.model.is_dn:
            self.update_dn = theano.function(
                    inputs=[],
                    outputs=[],
                    updates=self.model.update_for_dn,
                    on_unused_input='warn')

        if self.is_prun:
            if not self.is_prun_songhan:
                # the set of function used to update pruning matrices after each epoch
                self.update_pi = theano.function(
                    inputs=[self.model.x_unl, self.model.is_train, self.model.momentum_bn],
                    outputs=self.model.pi_minibatch_vars,
                    updates=self.model.updates_pi,
                    on_unused_input='warn')

                self.correct_pi = theano.function(
                    inputs=[],
                    outputs=[],
                    updates=self.model.correct_pi,
                    on_unused_input='warn')

                self.update_pi_old = theano.function(
                    inputs=[],
                    outputs=[],
                    updates=self.model.updates_pi_old,
                    on_unused_input='warn')

            self.update_pruning = theano.function(
                inputs=[self.model.prun_threshold, self.model.prun_weights],
                outputs=[],
                updates=self.model.updates_pruning,
                on_unused_input='warn')

        # Initialize dictionaries to hold monitor costs, stats and param convergence
        cost_train_dict, var_stat_train_dict, cost_valid_dict, cost_test_dict, \
        param_stat_iter_dict, param_stat_epoch_dict, \
        param_convergence_iter_dict, param_convergence_epoch_dict, \
        param_iter_dict, param_epoch_dict = self.initialize_all_dicts()

        # epoch_vec and iter_vec
        epoch_vec = []; epoch = 0; epoch_vec.append(epoch)
        iter_vec = []; iter = 0; iter_vec.append(iter)
        percent_pruned_epoch_vec = []; percent_pruned_epoch_vec.append(0.)
        percent_pruned_iter_vec = []; percent_pruned_iter_vec.append(0.)
        percent_pruned_synap_epoch_vec = []; percent_pruned_synap_epoch_vec.append(0.)
        percent_pruned_synap_iter_vec = []; percent_pruned_synap_iter_vec.append(0.)

        # Compute initial validation and test costs, stats
        self.monitor_for_test_validation(cost_valid_dict=cost_valid_dict,
                                         cost_test_dict=cost_test_dict,
                                         epoch=epoch)

        # set up parameters for the training loop
        done_looping = False

        # compute decay val for the learning rate and the initial learning rate
        decay_val = np.exp(np.log(self.lr_init / self.lr_final) / (self.max_epochs - 2))
        current_lr = self.lr_init * decay_val

        # compute pruning val for the pruning rate and the initial pruning rate
        current_prun_rate = [self.prun_rates[0] for l in range(len(self.model.layer_names))]
        count_prun = [0 for l in range(len(self.model.layer_names))]

        # Save initial models
        self.save_model_epoch(epoch=epoch, current_lr=current_lr, current_prun_rate=current_prun_rate)

        while (epoch < self.max_epochs) and (not done_looping):
            # start_epoch = time.time()
            # collect params and params stacks that need to be monitored during the training (across epochs)
            self.track_params_and_params_stats(param_dict=param_epoch_dict, param_stat_dict=param_stat_epoch_dict)

            if self.model.is_prun:
                # compute and keep track of percent of connections pruned in the model (per epoch)
                self.track_percent_pruning_all_layers(param_stat_dict=param_stat_epoch_dict,
                                                      param_dict=param_epoch_dict, layer_names=self.model.all_layer_names,
                                                      percent_pruned_vec=percent_pruned_epoch_vec, index=epoch,
                                                      index_name='epoch', print_to='both')

                self.plot_curve(x_vec=epoch_vec, y_vec=percent_pruned_epoch_vec[0:-1],
                                x_name='epoch', y_name='percent_pruned', result_dir=self.cost_dir, markersize=0, linewidth=2)

                if self.is_plot_full:
                    self.plot_curve(x_vec=iter_vec[:-1], y_vec=percent_pruned_iter_vec[0:-1],
                                    x_name='iter', y_name='percent_pruned', result_dir=self.cost_dir, markersize=0,
                                    linewidth=2)

                # plot test error and prun
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_test_dict['classification-error'],
                                              prun_rate=percent_pruned_epoch_vec[0:-1], result_dir=self.cost_dir, name='test-error')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['classification-error'],
                                              prun_rate=percent_pruned_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-error')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['total-cost'],
                                              prun_rate=percent_pruned_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-total-cost')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['supervised-cost'],
                                              prun_rate=percent_pruned_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-supervised-cost')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['unsupervised-cost'],
                                              prun_rate=percent_pruned_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-unsupervised-cost')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['KLD-cost'],
                                              prun_rate=percent_pruned_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-KLD-cost')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['sign-cost'],
                                              prun_rate=percent_pruned_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-sign-cost')

            if self.model.is_prun_synap or self.is_prun_songhan:
                self.track_percent_synap_pruning_all_layers(param_stat_dict=param_stat_epoch_dict,
                                                            param_dict=param_epoch_dict,
                                                            layer_names=self.model.all_layer_names,
                                                            percent_pruned_vec=percent_pruned_synap_epoch_vec,
                                                            index=epoch, index_name='epoch', print_to='both')

                self.plot_curve(x_vec=epoch_vec, y_vec=percent_pruned_synap_epoch_vec[0:-1],
                                x_name='epoch', y_name='percent_pruned', result_dir=self.cost_dir, markersize=0, linewidth=2)

                if self.is_plot_full:
                    self.plot_curve(x_vec=iter_vec[:-1], y_vec=percent_pruned_synap_iter_vec[0:-1],
                                    x_name='iter', y_name='percent_pruned', result_dir=self.cost_dir, markersize=0,
                                    linewidth=2)

                # plot test error and prun
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_test_dict['classification-error'],
                                              prun_rate=percent_pruned_synap_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='test-error')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['classification-error'],
                                              prun_rate=percent_pruned_synap_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-error')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['total-cost'],
                                              prun_rate=percent_pruned_synap_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-total-cost')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['supervised-cost'],
                                              prun_rate=percent_pruned_synap_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-supervised-cost')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['unsupervised-cost'],
                                              prun_rate=percent_pruned_synap_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-unsupervised-cost')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['KLD-cost'],
                                              prun_rate=percent_pruned_synap_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-KLD-cost')
                self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_valid_dict['sign-cost'],
                                              prun_rate=percent_pruned_synap_epoch_vec[0:-1], result_dir=self.cost_dir,
                                              name='valid-sign-cost')

            # display best test and validation errors
            if self.model.is_prun:
                self.print_best_errors(cost_test_dict=cost_test_dict, cost_valid_dict=cost_valid_dict,
                                       percent_pruned_vec=percent_pruned_epoch_vec[0:-1], is_prun=True,
                                       is_prun_synap=False)
            elif self.model.is_prun_synap or self.is_prun_songhan:
                self.print_best_errors(cost_test_dict=cost_test_dict, cost_valid_dict=cost_valid_dict,
                                       percent_pruned_vec=percent_pruned_synap_epoch_vec[0:-1], is_prun=False,
                                       is_prun_synap=True)
            else:
                self.print_best_errors(cost_test_dict=cost_test_dict, cost_valid_dict=cost_valid_dict,
                                       percent_pruned_vec=[], is_prun=False, is_prun_synap=False)

            epoch = epoch + 1; epoch_vec.append(epoch) # update epoch

            train_dat, debug_info = self.siamese_dat.get_training_pair(batch_size=self.N_train, ratio=0.5,
                                                                       label_type="specific", K=None)

            # train_dat, debug_info = self.siamese_dat.get_superclass_training_pair(batch_size=self.N_train,
            #                                                                       sample_ratio=[0.5,0.1,0.4], K=None)

            # start the training for epoch i
            current_lr = np.float32(current_lr / decay_val)

            print('\nEpoch %d with Learning Rate = %f, Prun_Rate = %s' % (epoch, current_lr, str(current_prun_rate).strip('[]')))

            for minibatch_index in xrange(self.n_train_batches):
                if self.model.is_plot_histogram:
                    if iter % 10000 == 1:
                        if not self.is_plot_full:
                            # collect params and params stacks that need to be monitored during the training (across iters)
                            self.track_params_and_params_stats(param_dict=param_iter_dict,
                                                               param_stat_dict=param_stat_iter_dict)

                if self.is_plot_full:
                    # collect params and params stacks that need to be monitored during the training (across iters)
                    self.track_params_and_params_stats(param_dict=param_iter_dict,
                                                       param_stat_dict=param_stat_iter_dict)

                    if self.model.is_prun:
                        # compute and keep track of percent of connections pruned in the model (per iter)
                        self.track_percent_pruning_all_layers(param_stat_dict=param_stat_iter_dict,
                                                              param_dict=param_iter_dict,
                                                              layer_names=self.model.all_layer_names,
                                                              percent_pruned_vec=percent_pruned_iter_vec,
                                                              index=iter, index_name='iter', print_to='none')

                    if self.model.is_prun_synap:
                        self.track_percent_synap_pruning_all_layers(param_stat_dict=param_stat_iter_dict,
                                                                    param_dict=param_iter_dict,
                                                                    layer_names=self.model.all_layer_names,
                                                                    percent_pruned_vec=percent_pruned_synap_iter_vec,
                                                                    index=iter, index_name='iter', print_to='none')

                iter = iter + 1; iter_vec.append(iter) # update iter

                # construct randomly permuted minibatches

                if self.model.is_bn_BU:
                    output_val_train = self.train_batch(
                        train_dat['input1'][minibatch_index * self.batch_size:(minibatch_index + 1) * self.batch_size],
                        train_dat['input2'][minibatch_index * self.batch_size:(minibatch_index + 1) * self.batch_size],
                        train_dat['siamese-labels'][minibatch_index * self.batch_size:(minibatch_index + 1) * self.batch_size],
                        current_lr, 1, self.momentum_bn)
                else:
                    output_val_train = self.train_batch(
                        train_dat['input1'][minibatch_index * self.batch_size:(minibatch_index + 1) * self.batch_size],
                        train_dat['input2'][minibatch_index * self.batch_size:(minibatch_index + 1) * self.batch_size],
                        train_dat['siamese-labels'][
                        minibatch_index * self.batch_size:(minibatch_index + 1) * self.batch_size],
                        current_lr)

                if self.model.is_dn:
                    self.update_dn()

                # collect and plot train costs
                all_costs = output_val_train[-1]
                for i in range(len(self.model.monitor_cost_names)):
                    cost_train_dict[self.model.monitor_cost_names[i]].append(all_costs[i])

                if self.is_plot_full:
                    # compute and keep track of statistic of mornitored vars
                    self.compute_and_track_var_stats(var_stat_dict=var_stat_train_dict,
                                                     var_vec=output_val_train[:-1],
                                                     layer_names=self.model.all_layer_names,
                                                     var_names=self.model.monitor_var_names,
                                                     var_softmax_names=self.model.monitor_var_softmax_names,
                                                     stat_names=self.model.monitor_stat_names)

                    # compute and collect param convergences (across iter)
                    self.compute_and_track_param_convergences(param_dict=param_iter_dict,
                                                              param_convergence_dict=param_convergence_iter_dict,
                                                              layer_names=self.model.all_layer_names,
                                                              param_names=self.model.monitor_param_names,
                                                              param_softmax_names=self.model.monitor_param_softmax_names,
                                                              model_param_vec=self.model.monitor_params)
                if self.model.is_plot_histogram:
                    if iter % 10000 == 2:
                        # plot histograms
                        self.plot_histogram(var_vec=output_val_train[:-1], param_dict=param_iter_dict, index_iter=iter, result_dir=self.hist_dir)

            # Compute validation and test costs, stats in this epoch
            self.monitor_for_test_validation(cost_valid_dict=cost_valid_dict,
                                             cost_test_dict=cost_test_dict,
                                             epoch=epoch)

            # save intermediate models during training
            self.save_model_epoch(epoch=epoch, current_lr=current_lr, current_prun_rate=current_prun_rate)

            # save the latest model
            self.save_model(name='latest', current_lr=current_lr, current_prun_rate=current_prun_rate)

            # save the model with the best test error
            if cost_test_dict['5-way-error'][-1] < np.min(cost_test_dict['5-way-error'][:-1]):
                self.save_model(name='best', current_lr=current_lr, current_prun_rate=current_prun_rate)

            if self.is_prun:
                self.prun_weights = np.asarray([(epoch % self.prun_freq[l] == 0) for l in range(len(self.model.layer_names))],
                                               dtype=np.int32)
                if np.sum(self.prun_weights) > 0 and np.any(np.asarray(count_prun) < (len(self.prun_rates) - 1)):
                    current_prun = []
                    if not self.is_prun_songhan:
                        self.update_pi_old()
                        train_dat_prun, debug_info = self.siamese_dat.get_training_pair(batch_size=1000,
                                                                                   ratio=0.5,
                                                                                   label_type="specific", K=None)
                        x_update_pruning = []
                        x_update_pruning.append(train_dat_prun['input1'])
                        x_update_pruning.append(train_dat_prun['input2'])
                        # x_update_pruning.append(train_dat_prun['input-lab'])
                        x_update_pruning = np.concatenate(x_update_pruning, axis=0)
                        N_random_batches = x_update_pruning.shape[0]/self.batch_size
                        for mini_indx in range(N_random_batches):
                            pi_minibatch_vals = self.update_pi(x_update_pruning[mini_indx * self.batch_size:(mini_indx + 1) * self.batch_size],
                                                               0, 1.0)

                        self.correct_pi()

                        for l in range(self.model.N_layer):
                            if self.prun_weights[l] == 1 and count_prun[l] <= (len(self.prun_rates) - 1):
                                pi_synap_value = self.model.layers[l].pi_synap.get_value()
                                mean_pi_synap = np.mean(pi_synap_value)
                                var_pi_synap = np.var(pi_synap_value)
                                alpha_conj = ((1. - mean_pi_synap)/var_pi_synap - 1./mean_pi_synap)*(mean_pi_synap**2)
                                beta_conj = alpha_conj*(1./mean_pi_synap - 1.)
                                current_prun.append(sp.stats.beta.ppf(current_prun_rate[l], alpha_conj, beta_conj))
                                count_prun[l] += 1
                                if count_prun[l] <= (len(self.prun_rates) - 1):
                                    current_prun_rate[l] = self.prun_rates[count_prun[l]]
                            else:
                                current_prun.append(0)

                        current_prun = np.asarray(current_prun, dtype=np.float32)

                    self.update_pruning(current_prun, np.float32(self.prun_weights))


            # compute and collect param convergences (across epoch)
            self.compute_and_track_param_convergences(param_dict=param_epoch_dict,
                                                      param_convergence_dict=param_convergence_epoch_dict,
                                                      layer_names=self.model.all_layer_names,
                                                      param_names=self.model.monitor_param_names,
                                                      param_softmax_names=self.model.monitor_param_softmax_names,
                                                      model_param_vec=self.model.monitor_params)

            if epoch % 1 == 0:
                # plot train figures for costs and stats
                self.plot_for_train(cost_train_dict=cost_train_dict, var_stat_train_dict=var_stat_train_dict, iter_vec=iter_vec)

                # plot validation and test figures for costs and stats
                self.plot_for_test_validation(cost_valid_dict=cost_valid_dict, cost_test_dict=cost_test_dict,
                                              epoch_vec=epoch_vec)
                # plot param figures for convergences and stats
                self.plot_for_params(param_convergence_iter_dict=param_convergence_iter_dict,
                                     param_convergence_epoch_dict=param_convergence_epoch_dict,
                                     param_stat_iter_dict=param_stat_iter_dict,
                                     param_stat_epoch_dict=param_stat_epoch_dict,
                                     iter_vec=iter_vec,
                                     epoch_vec=epoch_vec)

            # save all costs together with the index vectors for future plotting
            monitor_saved_dir = os.path.join(self.output_dir, 'monitor-vals-saved-with-index-vec.npz')
            np.savez(monitor_saved_dir, epoch_vec=epoch_vec, iter_vec=iter_vec,
                     cost_train_dict=cost_train_dict, cost_test_dict=cost_test_dict,
                     cost_valid_dict=cost_valid_dict,
                     param_convergence_dict=param_convergence_iter_dict,
                     param_convergence_epoch_dict=param_convergence_epoch_dict,
                     var_stat_train_dict=var_stat_train_dict,
                     param_stat_iter_dict=param_stat_iter_dict,
                     param_stat_epoch_dict=param_stat_epoch_dict,
                     percent_pruned_synap_epoch_vec=percent_pruned_synap_epoch_vec[0:-1],
                     percent_pruned_synap_iter_vec=percent_pruned_synap_iter_vec[0:-1],
                     percent_pruned_epoch_vec=percent_pruned_epoch_vec[0:-1],
                     percent_pruned_iter_vec=percent_pruned_iter_vec[0:-1]
                     )
            # end_epoch = time.time()
            # print('Training time per epoch = %f seconds'%(start_epoch - end_epoch))
            # time.sleep(100)

        print('Optimization complete.')
        # display best test and validation errors
        if self.model.is_prun:
            self.print_best_errors(cost_test_dict=cost_test_dict[:-1], cost_valid_dict=cost_valid_dict[:-1],
                                   percent_pruned_vec=percent_pruned_epoch_vec[0:-1], is_prun=True, is_prun_synap=False)
        elif self.model.is_prun_synap or self.is_prun_songhan:
            self.print_best_errors(cost_test_dict=cost_test_dict[:-1], cost_valid_dict=cost_valid_dict[:-1],
                                   percent_pruned_vec=percent_pruned_synap_epoch_vec[0:-1], is_prun=False, is_prun_synap=True)
        else:
            self.print_best_errors(cost_test_dict=cost_test_dict[:-1], cost_valid_dict=cost_valid_dict[:-1],
                                   percent_pruned_vec=[], is_prun=False, is_prun_synap=False)

    ####################################################################################################################
    # subroutines used in functions above
    ####################################################################################################################
    def plot_across_all_layers(self, epoch_vec, feature, name, legend_names):
        # a subroutine to plot figures
        fig = figure()
        plot(epoch_vec, np.asarray(feature))
        xlabel('Epoch')
        ylabel(name)
        legend(legend_names, prop={'size': 8})
        fig.savefig(os.path.join(self.output_dir, '%s.png'%name))
        close()

    def initialize_monitor_cost_dicts(self, cost_names):
        # initialize all cost dicts
        cost_dict = {}
        for i in range(len(cost_names)):
            cost_dict[cost_names[i]] = []

        return cost_dict

    def initialize_monitor_var_stat_dicts(self, layer_names, var_names, var_softmax_names, var_stat_names):
        # initialize all var dicts
        var_stat_dict = {}
        for l in range(len(layer_names)):
            var_stat_dict[layer_names[l]] = {}
            if layer_names[l] == 'softmax':
                var_names_for_layer = var_softmax_names
            else:
                var_names_for_layer = var_names

            for i in range(len(var_names_for_layer)):
                var_stat_dict[layer_names[l]][var_names_for_layer[i]] = {}
                for j in range(len(var_stat_names)):
                    var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]] = []

        return var_stat_dict

    def initialize_monitor_param_dicts(self, layer_names, param_names, param_softmax_names, param_stat_names):
        # initialize all param dicts
        param_stat_dict = {}
        for l in range(len(layer_names)):
            param_stat_dict[layer_names[l]] = {}
            if layer_names[l] == 'softmax':
                param_names_for_layer = param_softmax_names
            else:
                param_names_for_layer = param_names

            for i in range(len(param_names_for_layer)):
                param_stat_dict[layer_names[l]][param_names_for_layer[i]] = {}
                for j in range(len(param_stat_names)):
                    param_stat_dict[layer_names[l]][param_names_for_layer[i]][param_stat_names[j]] = []

        param_convergence_dict = {}
        for l in range(len(layer_names)):
            param_convergence_dict[layer_names[l]] = {}
            if layer_names[l] == 'softmax':
                param_names_for_layer = param_softmax_names
            else:
                param_names_for_layer = param_names

            for i in range(len(param_names_for_layer)):
                param_convergence_dict[layer_names[l]][param_names_for_layer[i]] = []

        param_dict = {}
        for l in range(len(layer_names)):
            param_dict[layer_names[l]] = {}
            if layer_names[l] == 'softmax':
                param_names_for_layer = param_softmax_names
            else:
                param_names_for_layer = param_names

            for i in range(len(param_names_for_layer)):
                param_dict[layer_names[l]][param_names_for_layer[i]] = []

        return param_stat_dict, param_convergence_dict, param_dict

    def initialize_all_dicts(self):
        cost_train_dict = self.initialize_monitor_cost_dicts(self.model.monitor_cost_names)
        var_stat_train_dict = self.initialize_monitor_var_stat_dicts(self.model.all_layer_names,
                                                                     self.model.monitor_var_names,
                                                                     self.model.monitor_var_softmax_names,
                                                                     self.model.monitor_stat_names)
        cost_valid_dict = self.initialize_monitor_cost_dicts(self.model.monitor_test_valid_cost_names)
        # var_stat_valid_dict = self.initialize_monitor_var_stat_dicts(self.model.all_layer_names,
        #                                                              self.model.monitor_var_names,
        #                                                              self.model.monitor_var_softmax_names,
        #                                                              self.model.monitor_stat_names)
        cost_test_dict = self.initialize_monitor_cost_dicts(self.model.monitor_test_valid_cost_names)
        # var_stat_test_dict = self.initialize_monitor_var_stat_dicts(self.model.all_layer_names,
        #                                                             self.model.monitor_var_names,
        #                                                             self.model.monitor_var_softmax_names,
        #                                                             self.model.monitor_stat_names)
        param_stat_iter_dict, param_convergence_iter_dict, param_iter_dict = self.initialize_monitor_param_dicts(
            self.model.all_layer_names,
            self.model.monitor_param_names,
            self.model.monitor_param_softmax_names,
            self.model.monitor_stat_names)

        param_stat_epoch_dict, param_convergence_epoch_dict, param_epoch_dict = self.initialize_monitor_param_dicts(
            self.model.all_layer_names,
            self.model.monitor_param_names,
            self.model.monitor_param_softmax_names,
            self.model.monitor_stat_names)

        return cost_train_dict, var_stat_train_dict, cost_valid_dict, cost_test_dict,\
               param_stat_iter_dict, param_stat_epoch_dict,\
               param_convergence_iter_dict, param_convergence_epoch_dict, \
               param_iter_dict, param_epoch_dict

    def construct_minibatches(self):
        trainx = []; trainy = []

        for t in range(int(np.ceil(self.trainx_unl.shape[0] / float(self.trainx_lab.shape[0])))):
            inds = self.rng.permutation(self.trainx_lab.shape[0])
            trainx.append(self.trainx_lab[inds])
            trainy.append(self.trainy_lab[inds])

        trainx = np.concatenate(trainx, axis=0)
        trainy = np.concatenate(trainy, axis=0)

        self.trainx_unl = self.trainx_unl[self.rng.permutation(self.trainx_unl.shape[0])]
        return trainx, trainy

    def compute_and_track_var_stats_costs(self, cost_dict, n_batches, dat_type):
        # Compute validation and test costs
        if dat_type == 'test':
            dat, debug_info = self.siamese_dat.get_test_triplet(batch_size=self.N_test, label_type="specific",
                                                                K=None, num_choices=5)
        else:
            dat, debug_info = self.siamese_dat.get_validation_triplet(batch_size=self.N_valid,
                                                                      label_type="specific", K=None,
                                                                      num_choices=5)
        #################################
        # self.visualize_input_images(dat=dat['reference'][0:20], output_dir=self.viz_dir, name='reference')
        # self.visualize_input_images(dat=dat['positive'][0:20], output_dir=self.viz_dir, name='positive')
        # self.visualize_input_images(dat=dat['negative'][0:20], output_dir=self.viz_dir, name='negative')
        # import pdb; pdb.set_trace()
        #################################
        full_sim_score_mat = []
        for mini_indx in range(n_batches):
            sim_score_mat = []
            for test_case in range(5):
                if self.model.is_bn_BU:
                    sim_score = self.test_val_batch(dat['image_ref'][mini_indx * self.batch_size:(mini_indx + 1) * self.batch_size],
                                                    (dat['image_choices'][test_case])[mini_indx * self.batch_size:(mini_indx + 1) * self.batch_size],
                                                    0, 1.0)
                else:
                    sim_score = self.test_val_batch(dat['image_ref'][mini_indx * self.batch_size:(mini_indx + 1) * self.batch_size],
                                                    (dat['image_choices'][test_case])[mini_indx * self.batch_size:(mini_indx + 1) * self.batch_size])
                sim_score_mat.append(sim_score)

            sim_score_mat = np.asarray(sim_score_mat)
            full_sim_score_mat.append(sim_score_mat)

        full_sim_score_mat = np.concatenate(full_sim_score_mat, axis=1)
        pred_results = np.argmax(full_sim_score_mat, axis=0)
        error_val = np.mean((pred_results!=dat['answer']).astype(np.float32))

        cost_dict['5-way-error'].append(error_val)

    def visualize_input_images(self, dat, output_dir, name):
        imgs = np.reshape(dat,(0, 2, 3, 1))
        N = imgs.shape[0]
        for i in xrange(N):
            if imgs.shape[3] == 1:
                imshow(imgs[i, :, :, 0], cmap='gray')
            axis('off')
            out_path = os.path.join(output_dir, name)
            if not os.path.exists(out_path):
                os.makedirs(out_path)

            out_file = os.path.join(out_path, '%s_%i.jpg' % (name, i))
            savefig(out_file)
            close()
        return


    def monitor_for_test_validation(self, cost_valid_dict, cost_test_dict, epoch):
        self.compute_and_track_var_stats_costs(cost_dict=cost_valid_dict,
                                               dat_type='valid',
                                               n_batches=self.n_valid_batches)

        self.compute_and_track_var_stats_costs(cost_dict=cost_test_dict,
                                               dat_type ='test',
                                               n_batches=self.n_test_batches)

        # display all test and validation costs after each epoch
        for i in range(len(self.model.monitor_test_valid_cost_names)):
            print('Validatition %s at epoch %i = %f' % (self.model.monitor_test_valid_cost_names[i], epoch,
                                                        cost_valid_dict[self.model.monitor_test_valid_cost_names[i]][-1]))
            print('Test %s at epoch %i = %f' % (self.model.monitor_test_valid_cost_names[i], epoch,
                                                cost_test_dict[self.model.monitor_test_valid_cost_names[i]][-1]))

            with open(self.output_dir + '/log.txt','a') as f:
                f.write('Validatition %s at epoch %i = %f\n' % (self.model.monitor_test_valid_cost_names[i], epoch,
                                                            cost_valid_dict[self.model.monitor_test_valid_cost_names[i]][-1]))
                f.write('Test %s at epoch %i = %f\n' % (self.model.monitor_test_valid_cost_names[i], epoch,
                                                    cost_test_dict[self.model.monitor_test_valid_cost_names[i]][-1]))


    def compute_and_track_var_stats(self, var_stat_dict, var_vec, layer_names, var_names, var_softmax_names, stat_names):
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                var_names_for_layer = var_softmax_names
            else:
                var_names_for_layer = var_names

            for i in range(len(var_names_for_layer)):
                new_batch = var_vec[l * len(var_names) + i]

                for j in range(len(stat_names)):
                    if stat_names[j] == 'min':
                        stat_val = np.min(new_batch)
                    elif stat_names[j] == 'max':
                        stat_val = np.max(new_batch)
                    elif stat_names[j] == 'mean':
                        stat_val = np.mean(new_batch)
                    elif stat_names[j] == 'var':
                        stat_val = np.var(new_batch)
                    elif stat_names[j] == 'sparsity':
                        stat_val = float(np.count_nonzero(new_batch))/float(np.size(new_batch))

                    var_stat_dict[layer_names[l]][var_names_for_layer[i]][stat_names[j]].append(stat_val)

    def collect_param_vals(self, param_dict, layer_names, param_names, param_softmax_names, model_param_vec):
        # collect params that need to be monitored during the training
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                param_names_for_layer = param_softmax_names
            else:
                param_names_for_layer = param_names

            for i in range(len(param_names_for_layer)):
                param_dict[layer_names[l]][param_names_for_layer[i]] \
                    = model_param_vec[l * len(param_names) + i].get_value()

    def compute_and_track_param_convergences(self, param_dict, param_convergence_dict, layer_names,
                                             param_names, param_softmax_names, model_param_vec):
        # compute and collect param convergences
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                param_names_for_layer = param_softmax_names
            else:
                param_names_for_layer = param_names

            for i in range(len(param_names_for_layer)):
                new_param_val = model_param_vec[l * len(param_names) + i].get_value()

                rel_norm_diff = norm(new_param_val - param_dict[layer_names[l]][param_names_for_layer[i]]) \
                                / norm(param_dict[layer_names[l]][param_names_for_layer[i]])
                param_convergence_dict[layer_names[l]][param_names_for_layer[i]].append(rel_norm_diff)

    def compute_and_track_param_stats(self, param_dict, param_stat_dict, layer_names, param_names, param_softmax_names, stat_names):
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                param_names_for_layer = param_softmax_names
            else:
                param_names_for_layer = param_names

            for i in range(len(param_names_for_layer)):
                for j in range(len(stat_names)):
                    if stat_names[j] == 'min':
                        stat_val = np.min(param_dict[layer_names[l]][param_names_for_layer[i]])
                    elif stat_names[j] == 'max':
                        stat_val = np.max(param_dict[layer_names[l]][param_names_for_layer[i]])
                    elif stat_names[j] == 'mean':
                        stat_val = np.mean(param_dict[layer_names[l]][param_names_for_layer[i]])
                    elif stat_names[j] == 'var':
                        stat_val = np.var(param_dict[layer_names[l]][param_names_for_layer[i]])
                    elif stat_names[j] == 'sparsity':
                        stat_val = float(np.count_nonzero(param_dict[layer_names[l]][param_names_for_layer[i]]))\
                                   /float(np.size(param_dict[layer_names[l]][param_names_for_layer[i]]))

                    param_stat_dict[layer_names[l]][param_names_for_layer[i]][stat_names[j]].append(stat_val)

    def track_params_and_params_stats(self, param_dict, param_stat_dict):
        self.collect_param_vals(param_dict=param_dict, layer_names=self.model.all_layer_names,
                                param_names=self.model.monitor_param_names,
                                param_softmax_names=self.model.monitor_param_softmax_names,
                                model_param_vec=self.model.monitor_params)

        self.compute_and_track_param_stats(param_dict=param_dict, param_stat_dict=param_stat_dict,
                                           layer_names=self.model.all_layer_names,
                                           param_names=self.model.monitor_param_names,
                                           param_softmax_names=self.model.monitor_param_softmax_names,
                                           stat_names=self.model.monitor_stat_names)

    def track_percent_pruning_all_layers(self, param_stat_dict, param_dict, layer_names,
                                         percent_pruned_vec, index, index_name, print_to='console'):
        # compute and keep track of percent of connections pruned in the model
        total_connections = 0
        pruned_connections = 0
        for l in xrange(len(layer_names) - 1):
            per_prun = 1 - param_stat_dict[layer_names[l]]['prun-mat']['sparsity'][-1]
            if print_to == 'console':
                print('Percent neuron pruning at layer %s = %f at %s %i' % (layer_names[l], per_prun, index_name, index))
            elif print_to == 'both':
                print('Percent neuron pruning at layer %s = %f at %s %i' % (layer_names[l], per_prun, index_name, index))
                with open(self.output_dir + '/log.txt','a') as f:
                    f.write('Percent neuron pruning at layer %s = %f at %s %i\n' % (layer_names[l], per_prun, index_name, index))

            total_connections += np.size(param_dict[layer_names[l]]['prun-mat']) * \
                                 self.model.layers[l].D
            pruned_connections += per_prun * (np.size(param_dict[layer_names[l]]['prun-mat'])
                                              * self.model.layers[l].D)

        percent_pruned_connections = pruned_connections / total_connections
        percent_pruned_vec.append(percent_pruned_connections)
        if print_to == 'console':
            print('Percent neuron pruning at %s %i = %f' % (index_name, index, percent_pruned_connections))
        elif print_to == 'both':
            print('Percent neuron pruning at %s %i = %f' % (index_name, index, percent_pruned_connections))
            with open(self.output_dir + '/log.txt','a') as f:
                f.write('Percent neuron pruning at %s %i = %f\n' % (index_name, index, percent_pruned_connections))

    def track_percent_synap_pruning_all_layers(self, param_stat_dict, param_dict, layer_names,
                                               percent_pruned_vec, index, index_name, print_to='console'):
        # compute and keep track of percent of connections pruned in the model
        total_connections = 0
        pruned_connections = 0
        for l in xrange(len(layer_names) - 1):
            per_prun = 1 - param_stat_dict[layer_names[l]]['prun-synap-mat']['sparsity'][-1]
            if print_to == 'console':
                print('Percent synapse pruning at layer %s = %f at %s %i' % (layer_names[l], per_prun, index_name, index))
            elif print_to == 'both':
                print('Percent synapse pruning at layer %s = %f at %s %i' % (layer_names[l], per_prun, index_name, index))
                with open(self.output_dir + '/log.txt','a') as f:
                    f.write('Percent synapse pruning at layer %s = %f at %s %i\n' % (layer_names[l], per_prun, index_name, index))

            total_connections += np.size(param_dict[layer_names[l]]['prun-synap-mat']) * \
                                 self.model.layers[l].latents_shape[2]*self.model.layers[l].latents_shape[3]
            pruned_connections += per_prun * (np.size(param_dict[layer_names[l]]['prun-synap-mat'])
                                              * self.model.layers[l].latents_shape[2]*self.model.layers[l].latents_shape[3])

        percent_pruned_connections = pruned_connections / total_connections
        percent_pruned_vec.append(percent_pruned_connections)
        if print_to == 'console':
            print('Percent synapse pruning at %s %i = %f' % (index_name, index, percent_pruned_connections))
        elif print_to == 'both':
            print('Percent synapse pruning at %s %i = %f' % (index_name, index, percent_pruned_connections))
            with open(self.output_dir + '/log.txt','a') as f:
                f.write('Percent synapse pruning at %s %i = %f\n' % (index_name, index, percent_pruned_connections))

    def increment_min(self, old_min, new_batch_min):
        return np.min((old_min, new_batch_min))

    def increment_max(self, old_max, new_batch_max):
        return np.max((old_max, new_batch_max))

    def increment_mean(self, old_mean, new_batch_mean, iter_indx):
        return (iter_indx * old_mean + new_batch_mean) / (iter_indx + 1)

    def increment_var(self, old_var, new_batch_var, old_mean, new_batch_mean, iter_indx):
        return (iter_indx * (old_var + old_mean ** 2) + (new_batch_var + new_batch_mean ** 2)) / (iter_indx + 1) \
               - ((iter_indx * old_mean + new_batch_mean) / (iter_indx + 1)) ** 2

    def increment_sparsity(self, old_spars, new_batch_spars, iter_indx):
        return (iter_indx * old_spars + new_batch_spars) / (iter_indx + 1)

    def append_init_stats(self, layer_names, var_names, var_softmax_names, var_stat_names, var_stat_dict):
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                var_names_for_layer = var_softmax_names
            else:
                var_names_for_layer = var_names

            for i in range(len(var_names_for_layer)):
                for j in range(len(var_stat_names)):
                    if var_stat_names[j] == 'min':
                        var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]].append(np.inf)
                    elif var_stat_names[j] == 'max':
                        var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]].append(-np.inf)
                    elif var_stat_names[j] == 'mean':
                        var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]].append(0)
                    elif var_stat_names[j] == 'var':
                        var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]].append(0)
                    elif var_stat_names[j] == 'sparsity':
                        var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]].append(0)


    def increment_stats(self, layer_names, var_names, var_softmax_names, var_stat_names, var_stat_dict, mini_indx, var_vec):
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                var_names_for_layer = var_softmax_names
            else:
                var_names_for_layer = var_names

            for i in range(len(var_names_for_layer)):
                new_batch = var_vec[l * len(var_names) + i]

                for j in range(len(var_stat_names)):
                    if var_stat_names[j] == 'min':
                        new_stat_val = self.increment_min(
                            old_min=var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]][-1],
                            new_batch_min=np.min(new_batch)
                        )
                    elif var_stat_names[j] == 'max':
                        new_stat_val = self.increment_max(
                            old_max=var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]][-1],
                            new_batch_max=np.max(new_batch)
                        )
                    elif var_stat_names[j] == 'var':
                        new_stat_val = self.increment_var(
                            old_var=var_stat_dict[layer_names[l]][var_names_for_layer[i]]['var'][-1],
                            new_batch_var=np.var(new_batch),
                            old_mean=var_stat_dict[layer_names[l]][var_names_for_layer[i]]['mean'][-1],
                            new_batch_mean=np.mean(new_batch),
                            iter_indx=mini_indx)
                    elif var_stat_names[j] == 'mean':
                        new_stat_val = self.increment_mean(
                            old_mean=var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]][-1],
                            new_batch_mean=np.mean(new_batch), iter_indx=mini_indx)
                    elif var_stat_names[j] == 'sparsity':
                        new_stat_val = self.increment_sparsity(
                            old_spars=var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]][-1],
                            new_batch_spars=np.count_nonzero(new_batch)/np.size(new_batch), iter_indx=mini_indx)

                    var_stat_dict[layer_names[l]][var_names_for_layer[i]][var_stat_names[j]][-1] = new_stat_val

    def save_meta_params(self, filename):
        # save meta parameters
        meta_dir = os.path.join(self.param_dir, filename)
        np.savez(meta_dir, lr_init=self.lr_init, lr_final=self.lr_final,
                 max_epochs=self.max_epochs, momentum_bn=self.momentum_bn,
                 batch_size=self.batch_size,
                 noise_std=self.model.noise_std, noise_weights=self.model.noise_weights,
                 reconst_weights=self.model.reconst_weights, is_bn_BU=self.model.is_bn_BU, is_bn_TD=self.model.is_bn_TD,
                 train_mode=self.model.train_mode, denoising=self.model.denoising,
                 grad_min=self.model.grad_min, grad_max=self.model.grad_max, is_tied_bn=self.model.is_tied_bn,
                 top_down_mode=self.model.top_down_mode, is_relu=self.model.is_TD_relu, nonlin=self.model.nonlin,
                 KL_coef=self.model.KL_coef, sign_cost_weight=self.model.sign_cost_weight, is_reluI=self.model.is_reluI,
                 is_end_to_end=self.model.is_end_to_end,
                 is_sample_c=self.model.is_sample_c, is_one_hot=self.model.is_one_hot)

    def save_model_epoch(self, epoch, current_lr, current_prun_rate):
        # save the model and the params decay during training
        misc_dir = os.path.join(self.param_dir, 'decayed-params-epoch-%i.npz' % epoch)
        np.savez(misc_dir, learning_rate=current_lr, current_prun_rate=current_prun_rate)

        model_dir = os.path.join(self.param_dir, 'model-epoch-%i.zip' % epoch)
        model_file = open(model_dir, 'wb')
        dump(self.model, model_file)
        model_file.close()

        model_dir = os.path.join(self.param_dir, 'model-epoch_%i.pkl' % epoch)
        model_file = open(model_dir, 'wb')
        cPickle.dump(self.model, model_file)
        model_file.close()

    def save_model(self, name, current_lr, current_prun_rate):
        # save the model and the params decay during training
        misc_dir = os.path.join(self.param_dir, 'decayed-params-%s.npz' % name)
        np.savez(misc_dir, learning_rate=current_lr, current_prun_rate=current_prun_rate)

        model_dir = os.path.join(self.param_dir, 'model-%s.zip' % name)
        model_file = open(model_dir, 'wb')
        dump(self.model, model_file)
        model_file.close()

        model_dir = os.path.join(self.param_dir, 'model-%s.pkl' % name)
        model_file = open(model_dir, 'wb')
        cPickle.dump(self.model, model_file)
        model_file.close()

    def plot_curve(self, x_vec, y_vec, x_name, y_name, result_dir, markersize=0, linewidth=2):
        # plot cost vs. epoch
        fig = figure()
        ax = plt.subplot(111)
        plot(x_vec, y_vec, 'bo-', markersize=markersize, linewidth=linewidth)
        xlabel('%s' % x_name)
        ylabel('%s' % y_name)
        ax.set_title('%s-vs-%s' % (y_name, x_name), fontsize=20)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_linewidth(3)
        ax.spines['left'].set_linewidth(3)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_fontsize(18)
        ax.yaxis.label.set_fontsize(18)
        ax.tick_params(axis='both', which='major', labelsize=20, length=8, width=2)
        ax.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        ax.set_xlim(left=0)
        grid()
        fig.savefig(os.path.join(result_dir, '%s-vs-%s.png' % (y_name, x_name)))
        close(fig)

    def plot_all_curves(self, monitor_names, x_vec, y_dict, x_name, extra_text, result_dir, markersize=0, linewidth=2):
        # plot all cost vs. epoch
        # this function calls plot_cost
        for i in range(len(monitor_names)):
            self.plot_curve(x_vec=x_vec, y_vec=y_dict[monitor_names[i]], x_name=x_name,
                            y_name=extra_text + '-' + monitor_names[i], result_dir=result_dir,
                            markersize=markersize, linewidth=linewidth)

    def plot_all_curves_all_layers(self, layer_names, monitor_names, monitor_softmax_names, x_vec, y_dict, x_name,
                                   extra_text, result_dir,
                                   markersize=0, linewidth=2):
        # plot all param curves vs. epoch/iter at all layers
        # this function calls plot_cost
        # the results include plots for each layer and a plot that contains curves of all layers
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                monitor_names_for_layer = monitor_softmax_names
                for i in range(len(monitor_names_for_layer)):
                    self.plot_curve(x_vec=x_vec[1:], y_vec=y_dict[layer_names[l]][monitor_names_for_layer[i]][1:], x_name=x_name,
                                    y_name=extra_text + '-' + layer_names[l] + '-' + monitor_names_for_layer[i], result_dir=result_dir,
                                    markersize=markersize, linewidth=linewidth)
            else:
                monitor_names_for_layer = monitor_names
                for i in range(len(monitor_names_for_layer)):
                    self.plot_curve(x_vec=x_vec, y_vec=y_dict[layer_names[l]][monitor_names_for_layer[i]], x_name=x_name,
                                    y_name=extra_text + '-' + layer_names[l] + '-' + monitor_names_for_layer[i], result_dir=result_dir,
                                    markersize=markersize, linewidth=linewidth)

        for i in range(len(monitor_names)):
            fig = figure()
            ax = plt.subplot(111)
            for l in range(len(layer_names)-1):
                plot(x_vec, y_dict[layer_names[l]][monitor_names[i]], 'o-', label=layer_names[l], markersize=markersize, linewidth=linewidth)
            legend()
            legend(prop={'size': 18})
            xlabel('%s' % x_name)
            ylabel('%s' % extra_text + '-' + monitor_names[i])
            ax.set_title('%s-vs-%s' % ('%s' % (extra_text + '-' + monitor_names[i]), x_name), fontsize=20)
            ax.spines['right'].set_visible(False)
            ax.spines['top'].set_visible(False)
            ax.spines['bottom'].set_linewidth(3)
            ax.spines['left'].set_linewidth(3)
            ax.yaxis.set_ticks_position('left')
            ax.xaxis.set_ticks_position('bottom')
            ax.xaxis.label.set_fontsize(18)
            ax.yaxis.label.set_fontsize(18)
            ax.tick_params(axis='both', which='major', labelsize=20, length=8, width=2)
            ax.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
            ax.set_xlim(left=0)
            grid()
            fig.savefig(os.path.join(result_dir, '%s-vs-%s.png' % ('%s' % (extra_text + '-' + monitor_names[i]), x_name)))
            close(fig)

    def plot_all_stat_curves_all_layers(self, layer_names, monitor_names, monitor_softmax_names,
                                        stat_names, x_vec, y_dict, x_name, extra_text, result_dir,
                                        markersize=0, linewidth=2):
        # plot all stat curves vs. epoch/iter at all layers
        # this function calls plot_cost
        # the results include plots for each layer and a plot that contains curves of all layers
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                monitor_names_for_layer = monitor_softmax_names
            else:
                monitor_names_for_layer = monitor_names

            for i in range(len(monitor_names_for_layer)):
                for j in range(len(stat_names)):
                    self.plot_curve(x_vec=x_vec, y_vec=y_dict[layer_names[l]][monitor_names_for_layer[i]][stat_names[j]], x_name=x_name,
                                    y_name=extra_text + '-' + layer_names[l] + '-' + stat_names[j] + '-' + monitor_names_for_layer[i],
                                    result_dir=result_dir,
                                    markersize=markersize, linewidth=linewidth)

        for i in range(len(monitor_names)):
            for j in range(len(stat_names)):
                fig = figure()
                ax = plt.subplot(111)
                for l in range(len(layer_names)-1):
                    plot(x_vec, y_dict[layer_names[l]][monitor_names[i]][stat_names[j]], 'o-', label=layer_names[l],
                         markersize=markersize, linewidth=linewidth)
                legend()
                legend(prop={'size': 18})
                xlabel('%s' % x_name)
                ylabel('%s' % extra_text + '-' + stat_names[j] + '-' + monitor_names[i])
                ax.set_title('%s-vs-%s' % ('%s' % (extra_text + '-' + stat_names[j] + '-' + monitor_names[i]), x_name),
                             fontsize=20)
                ax.spines['right'].set_visible(False)
                ax.spines['top'].set_visible(False)
                ax.spines['bottom'].set_linewidth(3)
                ax.spines['left'].set_linewidth(3)
                ax.yaxis.set_ticks_position('left')
                ax.xaxis.set_ticks_position('bottom')
                ax.xaxis.label.set_fontsize(18)
                ax.yaxis.label.set_fontsize(18)
                ax.tick_params(axis='both', which='major', labelsize=20, length=8, width=2)
                ax.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
                ax.set_xlim(left=0)
                grid()
                fig.savefig(os.path.join(result_dir,
                                         '%s-vs-%s.png' % ('%s' % (extra_text + '-' + stat_names[j] + '-' + monitor_names[i]),
                                                           x_name)))
                close(fig)

    def plot_test_error_and_prun(self, epoch_vec, error_rate, prun_rate, result_dir, name):
        fig = figure()
        ax = plt.subplot(111)
        plot(epoch_vec, error_rate, 'bo-', label=name, markersize=0, linewidth=2)
        hold(True)
        plot(epoch_vec, prun_rate, 'ro-', label='pruning-rate', markersize=0, linewidth=2)
        xlabel('Epoch')
        ylabel('Percent')
        legend(prop={'size': 18})
        ax.set_title('%s-and-pruning-rate-vs-epoch'% name, fontsize=24)
        ax.spines['right'].set_visible(False)
        ax.spines['top'].set_visible(False)
        ax.spines['bottom'].set_linewidth(3)
        ax.spines['left'].set_linewidth(3)
        ax.yaxis.set_ticks_position('left')
        ax.xaxis.set_ticks_position('bottom')
        ax.xaxis.label.set_fontsize(24)
        ax.yaxis.label.set_fontsize(24)
        ax.tick_params(axis='both', which='major', labelsize=20, length=8, width=2)
        ax.ticklabel_format(axis='y', style='sci', scilimits=(-2, 2))
        grid()
        fig.savefig(os.path.join(result_dir, '%s-and-pruning-rate-vs-epoch.png'% name))
        hold(False)
        close(fig)

    def plot_for_prun(self, percent_pruned_iter_vec, percent_pruned_epoch_vec, cost_test_dict, iter_vec, epoch_vec, result_dir):
        self.plot_curve(x_vec=iter_vec, y_vec=percent_pruned_iter_vec, result_dir=result_dir,
                        x_name='iter', y_name='percent_pruned', markersize=0, linewidth=2)
        self.plot_curve(x_vec=epoch_vec, y_vec=percent_pruned_epoch_vec, result_dir=result_dir,
                        x_name='epoch', y_name='percent_pruned', markersize=0, linewidth=2)
        self.plot_test_error_and_prun(epoch_vec=epoch_vec, error_rate=cost_test_dict['classification-error'],
                                      prun_rate=percent_pruned_epoch_vec, name='test-error')

    def plot_for_test_validation(self, cost_valid_dict, cost_test_dict, epoch_vec):
        # plot test and validation costs vs. epoch
        self.plot_all_curves(monitor_names=self.model.monitor_test_valid_cost_names, x_vec=epoch_vec,
                             y_dict=cost_valid_dict, x_name='epoch', extra_text='validation', result_dir=self.cost_dir,
                             markersize=0, linewidth=2)
        self.plot_all_curves(monitor_names=self.model.monitor_test_valid_cost_names, x_vec=epoch_vec,
                             y_dict=cost_test_dict, x_name='epoch', extra_text='test', result_dir=self.cost_dir,
                             markersize=0, linewidth=2)

    def plot_for_train(self, cost_train_dict, var_stat_train_dict, iter_vec):
        # plot train costs vs. iter
        self.plot_all_curves(monitor_names=self.model.monitor_cost_names, x_vec=iter_vec[1:],
                             y_dict=cost_train_dict, x_name='iter', extra_text='train', result_dir=self.cost_dir,
                             markersize=0, linewidth=2)

        # plot train var stats vs. iter
        if self.is_plot_full:
            self.plot_all_stat_curves_all_layers(layer_names=self.model.all_layer_names,
                                                 monitor_names=self.model.monitor_var_names,
                                                 monitor_softmax_names=self.model.monitor_var_softmax_names,
                                                 stat_names=self.model.monitor_stat_names,
                                                 x_vec=iter_vec[1:],
                                                 y_dict=var_stat_train_dict,
                                                 x_name='iter',
                                                 result_dir=self.stat_dir,
                                                 extra_text='train',
                                                 markersize=0, linewidth=2)

    def plot_for_params(self, param_convergence_iter_dict, param_convergence_epoch_dict,
                        param_stat_iter_dict, param_stat_epoch_dict, iter_vec, epoch_vec):

        if self.is_plot_full:
            # plot parameter convergences vs. iter
            self.plot_all_curves_all_layers(layer_names=self.model.all_layer_names,
                                            monitor_names=self.model.monitor_param_names,
                                            monitor_softmax_names=self.model.monitor_param_softmax_names,
                                            x_vec=iter_vec[1:],
                                            y_dict=param_convergence_iter_dict,
                                            x_name='iter',
                                            result_dir=self.convergence_dir,
                                            extra_text='convergence',
                                            markersize=0, linewidth=2)

        # plot parameter convergences vs. epoch
        self.plot_all_curves_all_layers(layer_names=self.model.all_layer_names,
                                        monitor_names=self.model.monitor_param_names,
                                        monitor_softmax_names=self.model.monitor_param_softmax_names,
                                        x_vec=epoch_vec[1:],
                                        y_dict=param_convergence_epoch_dict,
                                        x_name='epoch',
                                        result_dir=self.convergence_dir,
                                        extra_text='convergence',
                                        markersize=0, linewidth=2)

        if self.is_plot_full:
            # plot params stats vs. iter
            self.plot_all_stat_curves_all_layers(layer_names=self.model.all_layer_names,
                                                 monitor_names=self.model.monitor_param_names,
                                                 monitor_softmax_names=self.model.monitor_param_softmax_names,
                                                 stat_names=self.model.monitor_stat_names,
                                                 x_vec=iter_vec[:-1],
                                                 y_dict=param_stat_iter_dict,
                                                 x_name='iter',
                                                 result_dir=self.stat_dir,
                                                 extra_text='param',
                                                 markersize=0, linewidth=2)

        # plot params stats vs. epoch
        self.plot_all_stat_curves_all_layers(layer_names=self.model.all_layer_names,
                                             monitor_names=self.model.monitor_param_names,
                                             monitor_softmax_names=self.model.monitor_param_softmax_names,
                                             stat_names=self.model.monitor_stat_names,
                                             x_vec=epoch_vec[:-1],
                                             y_dict=param_stat_epoch_dict,
                                             x_name='epoch',
                                             result_dir=self.stat_dir,
                                             extra_text='param',
                                             markersize=0, linewidth=2)

    def plot_var_histogram(self, var_vec, layer_names, var_names, var_softmax_names, index_iter, result_dir):
        # plot histograms for all vars at all layers
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                var_names_for_layer = var_softmax_names
            else:
                var_names_for_layer = var_names

            for i in range(len(var_names_for_layer)):
                new_batch = var_vec[l * len(var_names) + i]
                random_indx = np.random.randint(0, np.shape(new_batch)[0], size=10)
                new_batch = new_batch[random_indx]

                hist, bins = np.histogram(new_batch.flatten(), bins=50)
                width = 0.7 * (bins[1] - bins[0])
                center = (bins[:-1] + bins[1:]) / 2
                plt.bar(center, hist, align='center', width=width)
                plt.title("Histogram of %s in %s at iter %i" % (var_names_for_layer[i], layer_names[l], index_iter))
                plt.savefig(os.path.join(result_dir, "histogram-of-%s-in-%s-at-iter-%i.png" % (
                var_names_for_layer[i], layer_names[l], index_iter)))
                plt.close()


    def plot_param_histogram(self, param_dict, layer_names, param_names, param_softmax_names, index_iter, result_dir):
        # plot histograms for all params at all layers
        for l in range(len(layer_names)):
            if layer_names[l] == 'softmax':
                param_names_for_layer = param_softmax_names
            else:
                param_names_for_layer = param_names

            for i in range(len(param_names_for_layer)):
                new_param = param_dict[layer_names[l]][param_names_for_layer[i]]

                hist, bins = np.histogram(new_param.flatten(), bins=50)
                width = 0.7 * (bins[1] - bins[0])
                center = (bins[:-1] + bins[1:]) / 2
                plt.bar(center, hist, align='center', width=width)
                plt.title("Histogram of %s in %s at iter %i" % (param_names_for_layer[i], layer_names[l], index_iter))
                plt.savefig(
                    os.path.join(result_dir, "histogram-of-%s-in-%s-at-iter-%i.png" % (param_names_for_layer[i],
                                                                                         layer_names[l], index_iter)))
                plt.close()

    def plot_histogram(self, var_vec, param_dict, index_iter, result_dir):
        # plot vars and params histograms
        self.plot_var_histogram(var_vec=var_vec, layer_names=self.model.all_layer_names,
                                var_names=self.model.monitor_var_names,
                                var_softmax_names=self.model.monitor_var_softmax_names,
                                index_iter=index_iter, result_dir=result_dir)
        self.plot_param_histogram(param_dict=param_dict, layer_names=self.model.all_layer_names,
                                  param_names=self.model.monitor_param_names,
                                  param_softmax_names=self.model.monitor_param_softmax_names,
                                  index_iter=index_iter, result_dir=result_dir)

    def print_best_errors(self, cost_test_dict, cost_valid_dict, percent_pruned_vec, is_prun, is_prun_synap):
        # display best test and validation errors with the correspondance percent prun
        if is_prun:
            print('Best test error of %f %% obtained at epoch %i with percent neuron pruned = %f %%' % (
                np.min(cost_test_dict['classification-error']) * 100.,
                np.argmin(cost_test_dict['classification-error']),
                percent_pruned_vec[np.argmin(cost_test_dict['classification-error'])]*100.))
            print('Best validation error of %f %% obtained at epoch %i with percent neuron pruned = %f %%' % (
                np.min(cost_valid_dict['classification-error']) * 100.,
                np.argmin(cost_valid_dict['classification-error']),
                percent_pruned_vec[np.argmin(cost_valid_dict['classification-error'])] * 100.))

            with open(self.output_dir + '/log.txt','a') as f:
                f.write('Best test error of %f %% obtained at epoch %i with percent neuron pruned = %f %%\n' % (
                    np.min(cost_test_dict['classification-error']) * 100.,
                    np.argmin(cost_test_dict['classification-error']),
                    percent_pruned_vec[np.argmin(cost_test_dict['classification-error'])]*100.))
                f.write('Best validation error of %f %% obtained at epoch %i with percent neuron pruned = %f %%\n' % (
                    np.min(cost_valid_dict['classification-error']) * 100.,
                    np.argmin(cost_valid_dict['classification-error']),
                    percent_pruned_vec[np.argmin(cost_valid_dict['classification-error'])] * 100.))

        elif is_prun_synap:
            print('Best test error of %f %% obtained at epoch %i with percent synap pruned = %f %%' % (
                np.min(cost_test_dict['classification-error']) * 100.,
                np.argmin(cost_test_dict['classification-error']),
                percent_pruned_vec[np.argmin(cost_test_dict['classification-error'])] * 100.))
            print('Best validation error of %f %% obtained at epoch %i with percent synap pruned = %f %%' % (
                np.min(cost_valid_dict['classification-error']) * 100.,
                np.argmin(cost_valid_dict['classification-error']),
                percent_pruned_vec[np.argmin(cost_valid_dict['classification-error'])] * 100.))

            with open(self.output_dir + '/log.txt','a') as f:
                f.write('Best test error of %f %% obtained at epoch %i with percent synap pruned = %f %%\n' % (
                    np.min(cost_test_dict['classification-error']) * 100.,
                    np.argmin(cost_test_dict['classification-error']),
                    percent_pruned_vec[np.argmin(cost_test_dict['classification-error'])] * 100.))
                f.write('Best validation error of %f %% obtained at epoch %i with percent synap pruned = %f %%\n' % (
                    np.min(cost_valid_dict['classification-error']) * 100.,
                    np.argmin(cost_valid_dict['classification-error']),
                    percent_pruned_vec[np.argmin(cost_valid_dict['classification-error'])] * 100.))
        else:
            print('Best test error of %f %% obtained at epoch %i' % (
                np.min(cost_test_dict['5-way-error']) * 100.,
                np.argmin(cost_test_dict['5-way-error'])))
            print('Best validation error of %f %% obtained at epoch %i' % (
                np.min(cost_valid_dict['5-way-error']) * 100.,
                np.argmin(cost_valid_dict['5-way-error'])))

            with open(self.output_dir + '/log.txt','a') as f:
                f.write('Best test error of %f %% obtained at epoch %i\n' % (
                    np.min(cost_test_dict['5-way-error']) * 100.,
                    np.argmin(cost_test_dict['5-way-error'])))
                f.write('Best validation error of %f %% obtained at epoch %i\n' % (
                    np.min(cost_valid_dict['5-way-error']) * 100.,
                    np.argmin(cost_valid_dict['5-way-error'])))

    # debug = theano.function(
    #     inputs=[self.model.x_unl, self.model.is_train, self.model.momentum_bn],
    #     outputs=[self.model.topk, self.model.topk_posteriors],
    #     updates=[],
    #     on_unused_input='warn')