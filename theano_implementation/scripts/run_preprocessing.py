import os
import pickle
import random

import numpy as np

from theano_implementation.simdrmm import preprocessing

base = "/home/ubuntu/shapenet-data/"
path = os.path.join("/home/ubuntu/shapenet-data/data_split_remake.pkl")
data = pickle.load(open(path, "rb"))
X = data["X"]
Y = data["Y"]
Yg = data["Yg"]
"""
X_train = X[0:round(X.shape[0]*0.8),:]
X_test = X[round(X_train.shape[0]):,:]

Y_train = Y[0:round(X.shape[0]*0.8),:]
Y_test = Y[round(X_train.shape[0]):,:]

Yg_train = Yg[0:round(X.shape[0]*0.8),:]
Yg_test = Yg[round(X_train.shape[0]):,:]


print X_train.shape
print X_test.shape
print Y_train.shape
print Y_test.shape

X_train_preprocessed, X_test_preprocessed = preprocessing.forward_process( \
    X_train, X_test, base, tag="_96")

np.savez(os.path.join(base, "data_96.npz"), X_train=X_train, \
    X_test=X_test, Y_train=Y_train, Y_test=Y_test, Yg_train=Yg_train, Yg_test=Yg_test)
np.savez(os.path.join(base, "data_preprocessed_96.npz"), X_train=X_train_preprocessed, \
    X_test=X_test_preprocessed, Y_train=Y_train, Y_test=Y_test, Yg_train=Yg_train, Yg_test=Yg_test)
"""

X_train = X[0]
X_test = X[1]
X_valid = X[2]

Y_train = Y[0]
Y_test = Y[1]
Y_valid = Y[2]

Yg_train = Yg[0]
Yg_test = Yg[1]
Yg_valid = Yg[2]


print X_train.shape
print X_test.shape
print X_valid.shape

print Y_train.shape
print Y_test.shape
print Y_valid.shape

print "Total Ys:",np.unique(np.vstack(Y)).size
print "Total Ygs:",np.unique(np.vstack(Yg)).size

X_train_preprocessed, X_test_preprocessed, X_valid_preprocessed = preprocessing.forward_process( \
    X_train, X_test,base, X_valid=X_valid, tag="_data_split")

tr = range(X_train.shape[0])
te = range(X_test.shape[0])
va = range(X_valid.shape[0]) 

random.shuffle(tr)
random.shuffle(te)
random.shuffle(va)

X_train = X_train[tr,:]
X_test = X_test[te,:]
X_valid = X_valid[va,:]

X_train_preprocessed = X_train_preprocessed[tr,:]
X_test_preprocessed = X_test_preprocessed[te,:]
X_valid_preprocessed = X_valid_preprocessed[va,:]


Y_train = Y_train[tr,:]
Y_test = Y_test[te,:]
Y_valid = Y_valid[va,:]

Yg_train = Yg_train[tr,:]
Yg_test = Yg_test[te,:]
Yg_valid = Yg_valid[va,:]


#np.savez(os.path.join(base, "data_split_remake.npz"), X_train=X_train, \
#    X_test=X_test, X_valid=X_valid, Y_train=Y_train, Y_test=Y_test, Y_valid=Y_valid,Yg_train=Yg_train, Yg_test=Yg_test, Yg_valid=Yg_valid)
#

#np.savez(os.path.join(base, "data_split_preprocessed_remake.npz"), X_train=X_train_preprocessed, \
#    X_test=X_test_preprocessed, X_valid=X_valid_preprocessed, Y_train=Y_train, Y_test=Y_test, Y_valid=Y_valid,Yg_train=Yg_train, Yg_test=Yg_test, Yg_valid=Yg_valid)


