# Copyright 2014, Sandia Corporation. Under the terms of Contract
# DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains certain
# rights in this software.

from setuptools import setup, find_packages
import re

setup(
    description = "pylearn 2",
    install_requires = [],
    maintainer_email = "",
    maintainer = "",
    name = "pylearn2",
    packages=find_packages(),
    version='',
)
